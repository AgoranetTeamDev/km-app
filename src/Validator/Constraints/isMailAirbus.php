<?php

namespace App\Validator\Constraints;

use Symfony\Component\Validator\Constraint;


/**
 * @Annotation
 */
class isMailAirbus extends Constraint
{
    public $message = '"{{ string }}" is not a valid email.';

}