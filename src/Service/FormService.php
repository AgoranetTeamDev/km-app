<?php

namespace App\Service;


class FormService
{
    private $mailer;
    private $templating;
    private $mail_from;
    private $mail_to;

    public function __construct(\Swift_Mailer $mailer, \Twig_Environment $templating, $mail_from, $mail_to)
    {
        $this->mailer = $mailer;
        $this->templating = $templating;
        $this->mail_from = $mail_from;
        $this->mail_to = explode(',', $mail_to);
    }

    public function sendMail($name_template, $name_form, $entity, $mailto_add = null){

        $mailto = $this->mail_to;

        // ajout d'un destinataire
        if(!empty($mailto_add)) array_push($mailto, $mailto_add);

        $message = (new \Swift_Message('KM-APP - '. $name_form .' : Nouvelle soumission - '. $entity->getCreateAt()->format('Y-m-d H:i:s')))
            ->setFrom($this->mail_from)
            ->setTo($mailto)
            ->setBody(
                $this->templating->render(
                        'mail/'.$name_template.'.html.twig',
                        [
                            'name_form' => $name_form,
                            'date' => $entity->getCreateAt()->format('Y-m-d H:i:s'),
                            'entity' => $entity
                        ]
                    ),
        'text/html'
            );

        $this->mailer->send($message);
    }

}