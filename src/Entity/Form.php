<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\Context\ExecutionContextInterface;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use App\Validator\Constraints as AcmeAssert;

/**
 * @ORM\Entity(repositoryClass="App\Repository\FormRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class Form
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $type;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Assert\NotBlank(groups={"others"}, message="The parameters $_GET['title'] must not be empty.")
     */
    private $title;

    /**
     * @ORM\Column(type="datetime")
     * @Assert\DateTime
     */
    private $createAt;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Assert\Choice(callback="getOptKmSolution", message="Choose a valid value.")
     */
    private $kmSolution;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Assert\NotBlank(groups={"cop", "handover", "innov", "lg", "lesson", "others", "ckorm", "itv"})
     */
    private $requesterName;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @AcmeAssert\isMailAirbus
     * @Assert\NotBlank(groups={"cop", "handover", "innov", "lg", "lesson", "others", "ckorm", "itv"})
     */
    private $requesterEmail;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @AcmeAssert\isMailAirbus
     */
    private $managerEmail;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Assert\Choice(callback="getOptDivision", message="Choose a valid value.")
     * @Assert\NotBlank()
     */
    private $division;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Assert\Choice(callback="getOptCountry", message="Choose a valid value.")
     * @Assert\NotBlank()
     */
    private $country;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Assert\NotBlank()
     */
    private $siglium;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Assert\NotBlank()
     */
    private $site;

    /**
     * @ORM\Column(type="text", nullable=true)
     * @Assert\NotBlank(groups={"cop", "handover", "innov", "lg", "lesson", "others"})
     */
    private $context;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Assert\Choice(callback="getOptContextChoice", message="Choose a valid value.")
     * @Assert\NotBlank(groups={"ckt"})
     */
    private $choiceContext;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Assert\Choice(callback="getOptLangue", message="Choose a valid value.")
     * @Assert\NotBlank
     */
    private $langue;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $is_manager;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Assert\NotBlank(groups={"ckt"})
     */
    private $giverName;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @AcmeAssert\isMailAirbus
     * @Assert\NotBlank(groups={"ckt"})
     */
    private $giverEmail;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @AcmeAssert\isMailAirbus
     */
    private $giverEmailManager;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @AcmeAssert\isMailAirbus
     * @Assert\NotBlank(groups={"handover"})
     */
    private $leaverEmail;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @AcmeAssert\isMailAirbus
     */
    private $leaverEmailManager;

    /**
     * @ORM\Column(type="date", nullable=true)
     * @Assert\Date(groups={"ckt", "itv"})
     * @Assert\NotNull(groups={"itv"})
     * @Assert\GreaterThan("yesterday", groups={"ckt","itv"})
     * @Assert\Expression(
     *     "this.getStartDate() <= this.getEndDate()",
     *     groups={"ckt"},
     *     message="The Transfer start date must be earlier than or equal to the End of transfer"
     * )
     * @Assert\Expression(
     *     "this.getStartDate() <= this.getEndDate()",
     *     groups={"itv"},
     *     message="The Possible start date must be earlier than or equal to the Objective delivery date"
     * )
     */
    private $startDate;

    /**
     * @ORM\Column(type="date", nullable=true)
     * @Assert\Date(groups={"ckt", "itv"})
     * @Assert\NotNull(groups={"ckt"})
     * @Assert\GreaterThan("yesterday", groups={"ckt", "itv"})
     */
    private $endDate;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $comment;

    /**
     * @ORM\Column(type="date", nullable=true)
     * @Assert\Date(groups={"handover"})
     * @Assert\NotNull(groups={"handover"})
     */
    private $leaverDate;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Assert\Choice(callback="getOptWorkshopType", message="Choose a valid value.")
     * @Assert\NotBlank(groups={"innov"})
     */
    private $choiceWorkshopType;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $workshopType;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Assert\Choice(callback="getOptSolution", message="Choose a valid value.")
     * @Assert\NotBlank(groups={"itv"})
     */
    private $choiceSolution;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Assert\Choice(callback="getOptWorkshopTypeDuration", message="Choose a valid value.")
     * @Assert\NotBlank(groups={"lesson"})
     */
    private $workshopTypeDuration;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Assert\NotBlank(groups={"innov", "lesson"})
     */
    private $proposedWeeks;

    /**
     * @ORM\Column(type="integer", nullable=true)
     * @Assert\Type(type="digit")
     * @Assert\NotBlank(groups={"innov", "lg", "lesson"})
     */
    private $nbParticipants;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Assert\Choice(callback="getOptExpertCko", message="Choose a valid value.")
     * @Assert\NotBlank(groups={"ckt"})
     */
    private $expertCko;

    /**
     * @ORM\Column(type="array", nullable=true)
     */
    private $nameReceiverSuccessor = [];



    CONST SUFFIX_MAIL_AIRBUS = '@airbus.com';
    CONST SUBMIT_BUTTON = 'Send this request';
    CONST ADD_BUTTON = 'Add other';
    CONST MSG_SUCCESS = 'Form submitted!';

    CONST LABEL_LANGUE = 'Preferred oral facilitation language';
    CONST LABEL_REQUESTER_NAME = 'Requester name';
    CONST LABEL_REQUESTER_EMAIL = 'Requester email address';
    CONST LABEL_REQUESTER_NAME_GIVER = 'Your email as requester';
    CONST LABEL_MANAGER_EMAIL = 'Manager email address';
    CONST LABEL_GIVER_NAME = 'Giver name';
    CONST LABEL_GIVER_EMAIL = 'Giver email address';
    CONST LABEL_GIVER_EMAIL_MANAGER = 'Giver\'s manager email';
    CONST LABEL_DIVISION = 'Division';
    CONST LABEL_COUNTRY = 'Country';
    CONST LABEL_SIGLIUM = 'Siglum';
    CONST LABEL_SITE = 'Site';
    CONST LABEL_CONTEXT = 'Context';
    CONST LABEL_CONTEXT_OTHER = 'Other context';
    CONST LABEL_OTHER_INFO = 'Other information / comment, not addressed above';
    CONST LABEL_IS_MANAGER = 'I\'m the manager';
    CONST LABEL_IS_GIVER_MANAGER = 'I\'m the giver\'s manager';
    CONST LABEL_IS_LEAVER_MANAGER = 'I\'m the leaver\'s manager';
    CONST LABEL_PROPOSED_WEEKS = 'Proposed week(s)';
    CONST LABEL_NB_PARTICIPANTS = 'Number of participants';
    CONST LABEL_LEAVER_EMAIL = 'Leaver email address';
    CONST LABEL_LEAVER_EMAIL_MANAGER= 'Leaver’s manager email address';
    CONST LABEL_LEAVER_DEPARTURE_DATE = 'Leaver Departure date';
    CONST LABEL_SUCCESSORS_NAMES = 'Successor(s) name(s)';
    CONST LABEL_END_DATE_TRANSFER = 'Giver departure / end of transfer';
    CONST LABEL_START_DATE_TRANSFER = 'Transfer start date';
    CONST LABEL_EXPERT_CKO = 'Expert, Critical Knowledge Owner, or other?';
    CONST LABEL_RECEIVERS_NAMES = 'Receiver(s) name(s), if known at request date';
    CONST LABEL_COMMENT = 'Comment';
    CONST LABEL_KM_SOLUTION = 'I request the following KM Solution';
    CONST LABEL_WORKSHOP_TYPE = 'Workshop type';
    CONST LABEL_WORKSHOP_TYPE_OTHER = 'Other workshop type';
    CONST LABEL_WORKSHOP_TYPE_DURATION = 'Workshop type and duration';
    CONST LABEL_CHOICE_SOLUTION = 'Choice of the solution';
    CONST LABEL_START_DATE_POSSIBLE = 'Possible start date';
    CONST LABEL_END_DATE_OBJECTIVE = 'Objective delivery date';


    CONST HELP_KM_SOLUTION_SELECT = 'Select one of the proposed solutions';
    CONST HELP_REQUESTER_NAME = 'First name, Family name of Requester';
    CONST HELP_REQUESTER_EMAIL= 'eMail address of requester';
    CONST HELP_LANGUE = 'English if left empty. All deliverables will be in English';
    CONST HELP_END_DATE_TRANSFER = 'Giver\'s estimated, or defined departure date (dd/mm/yyyy), or required end of transfer without departure of the giver.';
    CONST HELP_START_DATE_TRANSFER  = '
                    We recommend a minimum of:
                    <ul>
    <li>6 months before retirement, or in case of complex transfer (e.g. wide and multi-domain scope, several receivers)</li>
    <li>typically 3 months in other cases</li>
    </ul>';
    CONST HELP_EXPERT_CKO = 'Is the Giver Expert (Expert Career Path), or identified as Critical Knowledge Owner, or other?';
    CONST HELP_RECEIVERS_NAMES = 'First name, Family name, if known';
    CONST HELP_CONTEXT = 'Select one reason in the list, or give another one';
    CONST HELP_OTHER_INFO = 'Any other information, or comment that would help the solution provider to adapt to your needs.';
    CONST HELP_CONTEXT_EG = 'E.g. A/C Programme, Project (Free text)';
    CONST HELP_WORKSHOP_TYPE_DURATION = 'Choose the requested';
    CONST HELP_LEAVER_DEPARTURE_DATE = 'Leaver\'s estimated, or defined';
    CONST HELP_DIVISION = 'Leaver Division';
    CONST HELP_SIGLUM = 'Leaver Siglum';

    CONST FORM_COP = 'cop';
    CONST FORM_CKT = 'ckt';
    CONST FORM_HANDOVER = 'handover';
    CONST FORM_INNOV = 'innov';
    CONST FORM_LG = 'lg';
    CONST FORM_LESSON = 'lesson';
    CONST FORM_OTHERS = 'others';
    CONST FORM_CKORM = 'ckorm';
    CONST FORM_ITV = 'itv';


    public function getId(): ?int
    {
        return $this->id;
    }

    public function getType(): ?string
    {
        return $this->type;
    }

    public function setType(?string $type): self
    {
        $this->type = $type;

        return $this;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(?string $title): self
    {
        $this->title = $title;

        return $this;
    }

    public function getCreateAt(): ?\DateTimeInterface
    {
        return $this->createAt;
    }

    public function setCreateAt(?\DateTimeInterface $createAt): self
    {
        $this->createAt = $createAt;

        return $this;
    }

    public function getKmSolution(): ?string
    {
        return $this->kmSolution;
    }

    public function setKmSolution(?string $kmSolution): self
    {
        $this->kmSolution = $kmSolution;

        return $this;
    }

    public function getRequesterName(): ?string
    {
        return $this->requesterName;
    }

    public function setRequesterName(?string $requesterName): self
    {
        $this->requesterName = $requesterName;

        return $this;
    }

    public function getRequesterEmail(): ?string
    {
        return $this->requesterEmail;
    }

    public function setRequesterEmail(?string $requesterEmail): self
    {
        $this->requesterEmail = $requesterEmail;

        return $this;
    }

    public function getManagerEmail(): ?string
    {
        return $this->managerEmail;
    }

    public function setManagerEmail(?string $managerEmail): self
    {
        $this->managerEmail = $managerEmail;

        return $this;
    }

    public function getDivision(): ?string
    {
        return $this->division;
    }

    public function setDivision(?string $division): self
    {
        $this->division = $division;

        return $this;
    }

    public function getCountry(): ?string
    {
        return $this->country;
    }

    public function setCountry(?string $country): self
    {
        $this->country = $country;

        return $this;
    }

    public function getSiglium(): ?string
    {
        return $this->siglium;
    }

    public function setSiglium(?string $siglium): self
    {
        $this->siglium = $siglium;

        return $this;
    }

    public function getSite(): ?string
    {
        return $this->site;
    }

    public function setSite(?string $site): self
    {
        $this->site = $site;

        return $this;
    }

    public function getContext(): ?string
    {
        return $this->context;
    }

    public function setContext(?string $context): self
    {
        $this->context = $context;

        return $this;
    }

    public function getChoiceContext(): ?string
    {
        return $this->choiceContext;
    }

    public function setChoiceContext(?string $choiceContext): self
    {
        $this->choiceContext = $choiceContext;

        return $this;
    }

    public function getLangue(): ?string
    {
        return $this->langue;
    }

    public function setLangue(?string $langue): self
    {
        $this->langue = $langue;

        return $this;
    }

    public function getIsManager(): ?bool
    {
        return $this->is_manager;
    }

    public function setIsManager(?bool $is_manager): self
    {
        $this->is_manager = $is_manager;

        return $this;
    }

    public function getGiverName(): ?string
    {
        return $this->giverName;
    }

    public function setGiverName(?string $giverName): self
    {
        $this->giverName = $giverName;

        return $this;
    }

    public function getGiverEmail(): ?string
{
    return $this->giverEmail;
}

    public function setGiverEmail(?string $giverEmail): self
    {
        $this->giverEmail = $giverEmail;

        return $this;
    }

    public function getGiverEmailManager(): ?string
    {
        return $this->giverEmailManager;
    }

    public function setGiverEmailManager(?string $giverEmailManager): self
    {
        $this->giverEmailManager = $giverEmailManager;

        return $this;
    }

    public function getLeaverEmail(): ?string
    {
        return $this->leaverEmail;
    }

    public function setLeaverEmail(?string $leaverEmail): self
    {
        $this->leaverEmail = $leaverEmail;

        return $this;
    }

    public function getLeaverEmailManager(): ?string
    {
        return $this->leaverEmailManager;
    }

    public function setLeaverEmailManager(?string $leaverEmailManager): self
    {
        $this->leaverEmailManager = $leaverEmailManager;

        return $this;
    }

    public function getStartDate(): ?\DateTimeInterface
    {
        return $this->startDate;
    }

    public function setStartDate(?\DateTimeInterface $startDate): self
    {
        $this->startDate = $startDate;

        return $this;
    }

    public function getEndDate(): ?\DateTimeInterface
    {
        return $this->endDate;
    }

    public function setEndDate(?\DateTimeInterface $endDate): self
    {
        $this->endDate = $endDate;

        return $this;
    }

    public function getNaExpertCko1(): ?string
    {
        return $this->naExpertCko1;
    }

    public function setNaExpertCko1(?string $naExpertCko1): self
    {
        $this->naExpertCko1 = $naExpertCko1;

        return $this;
    }

    public function getNaExpertCko2(): ?string
    {
        return $this->naExpertCko2;
    }

    public function setNaExpertCko2(?string $naExpertCko2): self
    {
        $this->naExpertCko2 = $naExpertCko2;

        return $this;
    }

    public function getNaExpertCko3(): ?string
    {
        return $this->naExpertCko3;
    }

    public function setNaExpertCko3(?string $naExpertCko3): self
    {
        $this->naExpertCko3 = $naExpertCko3;

        return $this;
    }

    public function getComment(): ?string
    {
        return $this->comment;
    }

    public function setComment(?string $comment): self
    {
        $this->comment = $comment;

        return $this;
    }

    public function getLeaverDate(): ?\DateTimeInterface
    {
        return $this->leaverDate;
    }

    public function setLeaverDate(?\DateTimeInterface $leaverDate): self
    {
        $this->leaverDate = $leaverDate;

        return $this;
    }

    public function getChoiceWorkshopType(): ?string
    {
        return $this->choiceWorkshopType;
    }

    public function setChoiceWorkshopType(?string $choiceWorkshopType): self
    {
        $this->choiceWorkshopType = $choiceWorkshopType;

        return $this;
    }

    public function getWorkshopType(): ?string
    {
        return $this->workshopType;
    }

    public function setWorkshopType(?string $workshopType): self
    {
        $this->workshopType = $workshopType;

        return $this;
    }

    public function getChoiceSolution(): ?string
    {
        return $this->choiceSolution;
    }

    public function setChoiceSolution(?string $choiceSolution): self
    {
        $this->choiceSolution = $choiceSolution;

        return $this;
    }

    public function getProposedWeeks(): ?string
    {
        return $this->proposedWeeks;
    }

    public function setProposedWeeks(?string $proposedWeeks): self
    {
        $this->proposedWeeks = $proposedWeeks;

        return $this;
    }

    public function getNbParticipants(): ?string
    {
        return $this->nbParticipants;
    }

    public function setNbParticipants(?string $nbParticipants): self
    {
        $this->nbParticipants = $nbParticipants;

        return $this;
    }

    public function getWorkshopTypeDuration(): ?string
    {
        return $this->workshopTypeDuration;
    }

    public function setWorkshopTypeDuration(?string $workshopTypeDuration): self
    {
        $this->workshopTypeDuration = $workshopTypeDuration;

        return $this;
    }

    public function getExpertCko(): ?string
    {
        return $this->expertCko;
    }

    public function setExpertCko(?string $expertCko): self
    {
        $this->expertCko = $expertCko;

        return $this;
    }

    public function getNameReceiverSuccessor(): ?array
    {
        return $this->nameReceiverSuccessor;
    }

    public function setNameReceiverSuccessor(array $nameReceiverSuccessor): self
    {
        $this->nameReceiverSuccessor = $nameReceiverSuccessor;

        return $this;
    }

    // permet de savoir si la dateStart a 1 jour de difference en moins avec la dateEnd
    /*public function getDateStartLessDateEnd() {
        if($this->getStartDate() instanceof \DateTime){
            $diff = $this->getEndDate()->diff($this->getStartDate());
            // diff->invert == 1 si differencielle "-" sinon 0;
            return $diff->d == 0 || ($diff->invert == 1 && $diff->d >= 1);
        }else{
            return true;
        }
    }*/

    /**
     * @ORM\PrePersist()
     */
    public function prePersist() {
        $this->setCreateAt(new \DateTime());

        //permet de mettre l'extension '@airbus.com' aux champs listés ci-dessous
        $fields_mail = [
            'requesterEmail',
            'managerEmail',
            'giverEmail',
            'leaverEmail',
            'giverEmailManager',
            'leaverEmailManager',
        ];

        foreach($fields_mail as $field){
            if(!empty($this->{$field})) $this->{$field} = $this->{$field} . self::SUFFIX_MAIL_AIRBUS;
        }
    }

    public static function getOptKmSolution()
    {
        return [
            'Choose' => '',
            'Community of Pratice - Scoping' => 'Community of Pratice - Scoping',
            'Community of Pratice - Creation' => 'Community of Pratice - Creation',
            'Community of Pratice - Animation' => 'Community of Pratice - Animation',
            'Community of Pratice - Check up' => 'Community of Pratice - Check up',
        ];
    }

    public static function getOptSolution()
    {
        return [
            'Choose' => '',
            'Critical task identification' => 'Critical task identification',
            'Tutorial video creation' => 'Tutorial video creation',
            'Critical task identification & Tutorial video creation' => 'Critical task identification & Tutorial video creation'
        ];
    }

    public static function getOptLangue()
    {
        return [
            'English (default)' => 'English (default)',
            'French' => 'French',
            'German' => 'German',
            'Spanish' => 'Spanish',
        ];
    }

    public static function getOptCountry()
    {
        return [
            'Choose' => '',
            'France' => 'France',
            'Germany' => 'Germany',
            'United Kingdom' => 'United Kingdom',
            'Spain' => 'Spain',
            'America' => 'America',
            'WorldWide' => 'WorldWide',
        ];
    }

    public static function getOptExpertCko()
    {
        return [
            'Choose' => '',
            'Expert' => 'Expert Carrer Path',
            'Critical Knowledge Owner' => 'Critical Knowledge Owner',
            'Other' => 'Other Case',
        ];
    }

    public static function getOptContextChoice()
    {
        return [
            'Choose' => '',
            'Mobility' => 'Mobility',
            'Resignation' => 'Resignation',
            'Retirement' => 'Retirement',
            'Senior Agreement' => 'Senior Agreement',
            'Activity Transfer' => 'Activity Transfer',
            'Gemini' => 'Gemini',
            'Other' => 'Other',
        ];
    }

    public static function getOptWorkshopTypeDuration()
    {
        return [
            'Choose' => '',
            'LL Light (Half day)' => 'LL Light (Half day)',
            'LL Full (One day)' => 'LL Full (One day)',
        ];
    }

    public static function getOptWorkshopType()
    {
        return [
            'Choose' => '',
            'Creativity workshop' => 'Creativity workshop',
            'Design Thinking workshop' => 'Design Thinking workshop',
            'Business Thinking workshop' => 'Business Thinking workshop',
            'Other or to be defined' => 'Other',
        ];
    }

    public static function getOptDivision()
    {
        return [
            'Choose' => '',
            'AI' => 'AI',
            'AH' => 'AH',
            'ADS' => 'ADS'
        ];
    }
    
    /**
     * @Assert\Callback
    */
    public function validate(ExecutionContextInterface $context, $payload)
    {
        // champ "manager email"
        if (in_array($this->getType(), [self::FORM_COP, self::FORM_INNOV, self::FORM_LG, self::FORM_LESSON, self::FORM_OTHERS])){
            if($this->getIsManager() != true) {
                if(empty($this->getManagerEmail())){
                    $context->buildViolation('This value should not be blank.')
                        ->atPath('managerEmail')
                        ->addViolation();
                }
            }else{
                $this->setManagerEmail(null);
            }
        }

        // champ "giver manager email"
        if (in_array($this->getType() ,[self::FORM_CKT])){
            if($this->getIsManager() != true) {
                if(empty($this->getGiverEmailManager())){
                    $context->buildViolation('This value should not be blank.')
                        ->atPath('giverEmailManager')
                        ->addViolation();
                }
            }else{
                $this->setGiverEmailManager(null);
            }
        }

        // champ "leaver manager email"
        if (in_array($this->getType() ,[self::FORM_HANDOVER])){
            if($this->getIsManager() != true) {
                if(empty($this->getLeaverEmailManager())){
                    $context->buildViolation('This value should not be blank.')
                        ->atPath('leaverEmailManager')
                        ->addViolation();
                }
            }else{
                $this->setLeaverEmailManager(null);
            }
        }

        // champ "ChoiceContext - value other"
        if (in_array($this->getType(), [self::FORM_CKT])) {
            /*if ($this->getChoiceContext() == 'Other') {
                if (empty($this->getContext())) {
                    $context->buildViolation('This field must not be empty')
                        ->atPath('context')
                        ->addViolation();
                }
            } else {
                $this->setContext(null);
            }*/
            if ($this->getChoiceContext() != 'Other') {
                $this->setContext(null);
            }
        }

        // champ "choiceWorkshopType - value other"
        if (in_array($this->getType(), [self::FORM_INNOV])) {
            /*if ($this->getChoiceWorkshopType() == 'Other') {
                if (empty($this->getWorkshopType())) {
                    $context->buildViolation('This field must not be empty')
                        ->atPath('context')
                        ->addViolation();
                }
            } else {
                $this->setWorkshopType(null);
            }*/
            if ($this->getChoiceWorkshopType() != 'Other') {
                $this->setWorkshopType(null);
            }
        }

    }
}
