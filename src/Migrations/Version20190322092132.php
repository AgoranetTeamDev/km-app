<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190322092132 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE form DROP na_expert_cko1, DROP na_expert_cko2, DROP na_expert_cko3');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE form ADD na_expert_cko1 LONGTEXT DEFAULT NULL COLLATE utf8mb4_unicode_ci, ADD na_expert_cko2 LONGTEXT DEFAULT NULL COLLATE utf8mb4_unicode_ci, ADD na_expert_cko3 LONGTEXT DEFAULT NULL COLLATE utf8mb4_unicode_ci');
    }
}
