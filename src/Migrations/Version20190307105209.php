<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190307105209 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE form (id INT AUTO_INCREMENT NOT NULL, type VARCHAR(255) NOT NULL, title VARCHAR(255) DEFAULT NULL, create_at DATETIME NOT NULL, km_solution VARCHAR(255) DEFAULT NULL, requester_name VARCHAR(255) DEFAULT NULL, requester_email VARCHAR(255) DEFAULT NULL, manager_email VARCHAR(255) DEFAULT NULL, division VARCHAR(255) DEFAULT NULL, country VARCHAR(255) DEFAULT NULL, siglium VARCHAR(255) DEFAULT NULL, site VARCHAR(255) DEFAULT NULL, context LONGTEXT DEFAULT NULL, choice_context VARCHAR(255) DEFAULT NULL, langue VARCHAR(255) DEFAULT NULL, is_manager TINYINT(1) DEFAULT NULL, giver_name VARCHAR(255) DEFAULT NULL, giver_email VARCHAR(255) DEFAULT NULL, giver_email_manager VARCHAR(255) DEFAULT NULL, leaver_email VARCHAR(255) DEFAULT NULL, leaver_email_manager VARCHAR(255) DEFAULT NULL, start_date DATE DEFAULT NULL, end_date DATE DEFAULT NULL, na_expert_cko1 LONGTEXT DEFAULT NULL, na_expert_cko2 LONGTEXT DEFAULT NULL, na_expert_cko3 LONGTEXT DEFAULT NULL, comment LONGTEXT DEFAULT NULL, leaver_date DATE DEFAULT NULL, choice_workshop_type VARCHAR(255) DEFAULT NULL, workshop_type VARCHAR(255) DEFAULT NULL, choice_solution VARCHAR(255) DEFAULT NULL, workshop_type_duration VARCHAR(255) DEFAULT NULL, proposed_weeks VARCHAR(255) DEFAULT NULL, nb_participants INT DEFAULT NULL, expert_cko VARCHAR(255) DEFAULT NULL, name_receiver_successor LONGTEXT DEFAULT NULL COMMENT \'(DC2Type:array)\', PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP TABLE form');
    }
}
