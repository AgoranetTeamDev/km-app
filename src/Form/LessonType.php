<?php

namespace App\Form;

use App\Entity\Form;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;

class LessonType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('requesterName', TextType::class, [
                'label' => Form::LABEL_REQUESTER_NAME,
                'help' => Form::HELP_REQUESTER_NAME,
            ])
            ->add('requesterEmail', TextType::class, [
                'label' => Form::LABEL_REQUESTER_EMAIL,
                'attr' => [
                    'data-suffix' => Form::SUFFIX_MAIL_AIRBUS,
                ],
                'help' => Form::HELP_REQUESTER_EMAIL,
            ])
            ->add('is_manager', CheckboxType::class, [
                'label' => Form::LABEL_IS_MANAGER,
                'required' => false,
                'attr' => [
                    'data-toggle' => 'is_manager',
                ]
            ])
            ->add('managerEmail', TextType::class, [
                'label' => Form::LABEL_MANAGER_EMAIL,
                'required' => false,
                'attr' => [
                    'data-toggle-id' => 'is_manager',
                    'data-toggle-value' => '',
                    'data-toggle-checked' => 'show',
                    'data-toggle-required' => 'true',
                    'data-suffix' => Form::SUFFIX_MAIL_AIRBUS,
                ]
            ])
            ->add('division', ChoiceType::class, [
                'label' => Form::LABEL_DIVISION,
                'choices'  => Form::getOptDivision(),
            ])
            ->add('country', ChoiceType::class, [
                'label' => Form::LABEL_COUNTRY,
                'choices'  => Form::getOptCountry(),
            ])
            ->add('siglium', TextType::class, [
                'label' => Form::LABEL_SIGLIUM,
            ])
            ->add('site', TextType::class, [
                'label' => Form::LABEL_SITE,
            ])
            ->add('context', TextareaType::class, [
                'label' => Form::LABEL_CONTEXT,
                'help' => Form::HELP_CONTEXT_EG,
            ])
            ->add('workshopTypeDuration', ChoiceType::class, [
                'label' => Form::LABEL_WORKSHOP_TYPE_DURATION,
                'choices'  => Form::getOptWorkshopTypeDuration(),
                'help' => Form::HELP_WORKSHOP_TYPE_DURATION,
            ])
            ->add('proposedWeeks', TextType::class, [
                'label' => Form::LABEL_PROPOSED_WEEKS,
            ])
            ->add('nbParticipants', IntegerType::class, [
                'label' => Form::LABEL_NB_PARTICIPANTS,
            ])
            ->add('langue', ChoiceType::class, [
                'label' => Form::LABEL_LANGUE,
                'choices'  => Form::getOptLangue(),
                'help' => Form::HELP_LANGUE
            ])
            ->add('submit', SubmitType::class,[
                'label' => Form::SUBMIT_BUTTON,
            ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Form::class,
            'validation_groups' => ['Default','lesson'],
        ]);
    }
}
