<?php

namespace App\Form;

use App\Entity\Form;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;

class ItvType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('requesterName', TextType::class, [
                'label' => Form::LABEL_REQUESTER_NAME,
                'help' => Form::HELP_REQUESTER_NAME,
            ])
            ->add('requesterEmail', TextType::class, [
                'label' => Form::LABEL_REQUESTER_EMAIL,
                'help' => Form::HELP_REQUESTER_EMAIL,
                'attr' => [
                    'data-suffix' => Form::SUFFIX_MAIL_AIRBUS,
                ]
            ])
            ->add('managerEmail', TextType::class, [
                'label' => Form::LABEL_MANAGER_EMAIL,
                'attr' => [
                    'data-suffix' => Form::SUFFIX_MAIL_AIRBUS,
                ]
            ])
            ->add('division', ChoiceType::class, [
                'label' => Form::LABEL_DIVISION,
                'choices'  => Form::getOptDivision(),
            ])
            ->add('country', ChoiceType::class,[
                'label' => Form::LABEL_COUNTRY,
                'choices'  => Form::getOptCountry(),
            ])
            ->add('siglium', TextType::class,[
                'label' => Form::LABEL_SIGLIUM,
            ])
            ->add('site', TextType::class,[
                'label' => Form::LABEL_SITE,
            ])
            ->add('choiceSolution', ChoiceType::class, [
                'label' => Form::LABEL_CHOICE_SOLUTION,
                'choices'  => Form::getOptSolution(),
            ])

            ->add('startDate', DateType::class,[
                'label' => Form::LABEL_START_DATE_POSSIBLE,
                'years' => range(date('Y'), date('Y')+10),
                'data' => new \DateTime(),
            ])
            ->add('endDate', DateType::class,[
                'label' => Form::LABEL_END_DATE_OBJECTIVE,
                'years' => range(date('Y'), date('Y')+10),
                'data' => new \DateTime(),
            ])
            ->add('langue', ChoiceType::class,[
                'label' => Form::LABEL_LANGUE,
                'choices'  => Form::getOptLangue(),
                'help' => Form::HELP_LANGUE,
            ])
            ->add('comment', TextareaType::class,[
                'label' => Form::LABEL_COMMENT,
                'required' => false,
            ])
            ->add('submit', SubmitType::class,[
                'label' => Form::SUBMIT_BUTTON,
            ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Form::class,
            'validation_groups' => ['Default','itv'],
        ]);
    }
}
