<?php

namespace App\Form;

use App\Entity\Form;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;

class CkormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('requesterName', TextType::class, [
                'label' => Form::LABEL_REQUESTER_NAME,
                'help' => Form::HELP_REQUESTER_NAME,
            ])
            ->add('requesterEmail', TextType::class, [
                'label' => Form::LABEL_REQUESTER_EMAIL,
                'attr' => [
                    'data-suffix' => Form::SUFFIX_MAIL_AIRBUS,
                ],
                'help' => Form::HELP_REQUESTER_EMAIL,
            ])
            ->add('managerEmail', TextType::class, [
                'label' => Form::LABEL_MANAGER_EMAIL,
                'attr' => [
                    'data-suffix' => Form::SUFFIX_MAIL_AIRBUS,
                ]
            ])
            ->add('division', ChoiceType::class, [
                'label' => Form::LABEL_DIVISION,
                'choices'  => Form::getOptDivision(),
            ])
            ->add('country', ChoiceType::class, [
                'label' => Form::LABEL_COUNTRY,
                'choices'  => Form::getOptCountry(),
            ])
            ->add('siglium', TextType::class, [
                'label' => Form::LABEL_SIGLIUM,
            ])
            ->add('site', TextType::class, [
                'label' => Form::LABEL_SITE,
            ])
            ->add('langue', ChoiceType::class, [
                'label' => Form::LABEL_LANGUE,
                'choices'  => Form::getOptLangue(),
                'help' => Form::HELP_LANGUE
            ])
            ->add('comment', TextareaType::class, [
                'label' => Form::LABEL_COMMENT,
            ])
            ->add('submit', SubmitType::class,[
                'label' => Form::SUBMIT_BUTTON,
            ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Form::class,
            'validation_groups' => ['Default','ckorm'],
        ]);
    }
}
