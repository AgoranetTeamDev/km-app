<?php

namespace App\Form;

use App\Entity\Form;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

use Symfony\Component\Form\Extension\Core\Type\ButtonType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;

class HandoverType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('requesterName', TextType::class, [
                'label' => Form::LABEL_REQUESTER_NAME,
                'help' => Form::HELP_REQUESTER_NAME,
            ])
            ->add('requesterEmail', TextType::class, [
                'label' => Form::LABEL_REQUESTER_EMAIL,
                'attr' => [
                    'data-suffix' => Form::SUFFIX_MAIL_AIRBUS,
                ],
                'help' => Form::HELP_REQUESTER_EMAIL,
            ])
            ->add('is_manager', CheckboxType::class, [
                'label' => Form::LABEL_IS_LEAVER_MANAGER,
                'required' => false,
                'attr' => [
                    'data-toggle' => 'is_manager',
                ]
            ])
            ->add('leaverEmail', TextType::class, [
                'label' => Form::LABEL_LEAVER_EMAIL,
                'attr' => [
                    'data-suffix' => Form::SUFFIX_MAIL_AIRBUS,
                ]
            ])
            ->add('leaverEmailManager', TextType::class, [
                'label' => Form::LABEL_LEAVER_EMAIL_MANAGER,
                'required' => false,
                'attr' => [
                    'data-suffix' => Form::SUFFIX_MAIL_AIRBUS,
                    'data-toggle-id' => 'is_manager',
                    'data-toggle-value' => '',
                    'data-toggle-checked' => 'show',
                    'data-toggle-required' => 'true',
                ]
            ])
            ->add('leaverDate', DateType::class,[
                'label' => Form::LABEL_LEAVER_DEPARTURE_DATE,
                'help' => Form::HELP_LEAVER_DEPARTURE_DATE,
                'years' => range(date('Y'), date('Y')+10),
                'data' => new \DateTime(),
            ])
            ->add('division', ChoiceType::class, [
                'label' => Form::LABEL_DIVISION,
                'choices'  => Form::getOptDivision(),
                'help' => Form::HELP_DIVISION,
            ])
            ->add('country', ChoiceType::class, [
                'label' => Form::LABEL_COUNTRY,
                'choices'  => Form::getOptCountry(),
            ])
            ->add('siglium', TextType::class, [
                'label' => Form::LABEL_SIGLIUM,
                'help' => Form::HELP_SIGLUM,
            ])
            ->add('site', TextType::class, [
                'label' => Form::LABEL_SITE,
            ])
            ->add('nameReceiverSuccessor', CollectionType::class,[
                // each entry in the array will be an "email" field
                'label' => Form::LABEL_SUCCESSORS_NAMES,
                'required' => false,
                'entry_type' => TextType::class,
                'entry_options'  => [
                    'label' => false,
                ],
                'allow_add' => true,
                'allow_delete' => true,
                'prototype' => true,
                'attr' => [
                    'class' => 'multifield-dynamic',
                    'nb-item-init' => '1',
                ],
                'help' => Form::HELP_RECEIVERS_NAMES,
            ])
            ->add('addNameReceiverSuccessor', ButtonType::class,[
                'label' => Form::ADD_BUTTON,
                'attr' => [
                    'class' => 'add-another-collection-widget btn-primary',
                    'data-list-selector' => '#handover_nameReceiverSuccessor',
                ],
            ])
            ->add('context', TextareaType::class, [
                'label' => Form::LABEL_CONTEXT,
                'help' => Form::HELP_CONTEXT,
            ])
            ->add('langue', ChoiceType::class, [
                'label' => Form::LABEL_LANGUE,
                'choices'  => Form::getOptLangue(),
                'help' => Form::HELP_LANGUE
            ])
            ->add('comment', TextareaType::class,[
                'label' => Form::LABEL_OTHER_INFO,
                'required' => false,
                'help' => Form::HELP_OTHER_INFO,
            ])
            ->add('submit', SubmitType::class,[
                'label' => Form::SUBMIT_BUTTON,
            ]);
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Form::class,
            'validation_groups' => ['Default','handover'],
        ]);
    }
}
