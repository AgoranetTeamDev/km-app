<?php

namespace App\Form;

use App\Entity\Form;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;


class CopType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        $builder
            ->add('kmSolution', ChoiceType::class, [
                'label' => Form::LABEL_KM_SOLUTION,
                'choices'  => Form::getOptKmSolution(),
                'required' => false,
                'help' => Form::HELP_KM_SOLUTION_SELECT
            ])
            ->add('requesterName', TextType::class, [
                'label' => Form::LABEL_REQUESTER_NAME,
                'help' => Form::HELP_REQUESTER_NAME
            ])
            ->add('requesterEmail', TextType::class, [
                'label' => Form::LABEL_REQUESTER_EMAIL,
                'attr' => [
                    'data-suffix' => Form::SUFFIX_MAIL_AIRBUS,
                ],
                'help' => Form::HELP_REQUESTER_EMAIL,
            ])
            ->add('is_manager', CheckboxType::class, [
                'label' => Form::LABEL_IS_MANAGER,
                'required' => false,
                'attr' => [
                    'data-toggle' => 'is_manager',
                ]
            ])
            ->add('managerEmail', TextType::class, [
                'label' => Form::LABEL_MANAGER_EMAIL,
                'attr' => [
                    'data-suffix' => Form::SUFFIX_MAIL_AIRBUS,
                    'data-toggle-id' => 'is_manager',
                    'data-toggle-value' => '',
                    'data-toggle-checked' => 'show',
                    'data-toggle-required' => 'true',
                ]
            ])
            ->add('division', ChoiceType::class, [
                'label' => Form::LABEL_DIVISION,
                'choices'  => Form::getOptDivision(),
            ])
            ->add('country', ChoiceType::class,[
                'label' => Form::LABEL_COUNTRY,
                'choices'  => Form::getOptCountry(),
            ])
            ->add('siglium', TextType::class,[
                'label' => Form::LABEL_SIGLIUM,
            ])
            ->add('site', TextType::class,[
                'label' => Form::LABEL_SITE,
            ])
            ->add('context', TextareaType::class, [
                'label' => Form::LABEL_CONTEXT,
            ])
            ->add('langue', ChoiceType::class, [
                'label' => Form::LABEL_LANGUE,
                'choices'  => Form::getOptLangue(),
                'help' => Form::HELP_LANGUE
            ])
            ->add('submit', SubmitType::class,[
                'label' => Form::SUBMIT_BUTTON,
            ]);

    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Form::class,
            'validation_groups' => ['Default','cop'],
        ]);
    }
}
