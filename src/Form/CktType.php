<?php

namespace App\Form;

use App\Entity\Form;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\ButtonType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;

class CktType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        $builder
            ->add('giverName', TextType::class, [
                'label' => Form::LABEL_GIVER_NAME,
            ])
            ->add('giverEmail', TextType::class, [
                'label' => Form::LABEL_GIVER_EMAIL,
                'attr' => [
                    'data-suffix' => Form::SUFFIX_MAIL_AIRBUS,
                ]
            ])
            ->add('is_manager', CheckboxType::class, [
                'label' => Form::LABEL_IS_GIVER_MANAGER,
                'required' => false,
                'attr' => [
                    'data-toggle' => 'is_manager',
                ]
            ])
            ->add('requesterEmail', TextType::class, [
                'label' => Form::LABEL_REQUESTER_NAME_GIVER,
                'required' => false,
                'attr' => [
                    'data-suffix' => Form::SUFFIX_MAIL_AIRBUS,
                ]
            ])
            ->add('giverEmailManager', TextType::class, [
                'label' => Form::LABEL_GIVER_EMAIL_MANAGER,
                'attr' => [
                    'data-suffix' => Form::SUFFIX_MAIL_AIRBUS,
                    'data-toggle-id' => 'is_manager',
                    'data-toggle-value' => '',
                    'data-toggle-checked' => 'show',
                    'data-toggle-required' => 'true',
                ]
            ])
            ->add('endDate', DateType::class,[
                'label' => Form::LABEL_END_DATE_TRANSFER,
                'help' => Form::HELP_END_DATE_TRANSFER,
                'years' => range(date('Y'), date('Y')+10),
                'data' => new \DateTime(),
            ])
            ->add('startDate', DateType::class,[
                'label' => Form::LABEL_START_DATE_TRANSFER,
                'help' => Form::HELP_START_DATE_TRANSFER,
                'years' => range(date('Y'), date('Y')+10),
                'data' => new \DateTime(),
                'required' => false,
            ])
            ->add('expertCko', ChoiceType::class, [
                'label' => Form::LABEL_EXPERT_CKO,
                'choices'  => Form::getOptExpertCko(),
                'help' =>  Form::HELP_EXPERT_CKO,
            ])
            ->add('division', ChoiceType::class, [
                'label' => Form::LABEL_DIVISION,
                'choices'  => Form::getOptDivision(),
            ])
            ->add('country', ChoiceType::class,[
                'label' => Form::LABEL_COUNTRY,
                'choices'  => Form::getOptCountry(),
            ])
            ->add('siglium', TextType::class,[
                'label' => Form::LABEL_SIGLIUM,
            ])
            ->add('site', TextType::class,[
                'label' => Form::LABEL_SITE,
            ])
            ->add('nameReceiverSuccessor', CollectionType::class,[
                // each entry in the array will be an "email" field
                'label' => Form::LABEL_RECEIVERS_NAMES,
                'required' => false,
                'entry_type' => TextType::class,
                'entry_options'  => [
                    'label' => false,
                ],
                'allow_add' => true,
                'allow_delete' => true,
                'prototype' => true,
                'attr' => [
                    'class' => 'multifield-dynamic',
                    'nb-item-init' => '1',
                ],
                'help' =>  Form::HELP_RECEIVERS_NAMES,
            ])
            ->add('addNameReceiverSuccessor', ButtonType::class,[
                'label' => Form::ADD_BUTTON,
                'attr' => [
                    'class' => 'add-another-collection-widget btn-primary',
                    'data-list-selector' => '#ckt_nameReceiverSuccessor',
                ]
            ])
            ->add('choiceContext', ChoiceType::class,[
                'label' => Form::LABEL_CONTEXT,
                'choices'  => Form::getOptContextChoice(),
                'attr' => [
                    'data-toggle' => 'choiceContext',
                ],
                'help' =>  Form::HELP_CONTEXT,
            ])
            ->add('context', TextType::class,[
                'label' => Form::LABEL_CONTEXT_OTHER,
                'required' => false,
                'attr' => [
                    'data-toggle-id' => 'choiceContext',
                    'data-toggle-value' => 'Other',
                    'data-toggle-checked' => 'show',
                ]
            ])
            ->add('langue', ChoiceType::class,[
                'label' => Form::LABEL_LANGUE,
                'choices'  => Form::getOptLangue(),
                'help' => Form::HELP_LANGUE,
            ])
            ->add('comment', TextareaType::class,[
                'label' => Form::LABEL_OTHER_INFO,
                'required' => false,
                'help' => Form::HELP_OTHER_INFO,
            ])
            ->add('submit', SubmitType::class,[
                'label' => Form::SUBMIT_BUTTON,
            ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Form::class,
            'validation_groups' => ['Default','ckt'],
        ]);
    }
}
