<?php

namespace App\Controller;

use App\Entity\Form;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use App\Service\FormService;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\Routing\Annotation\Route;

use App\Form\CopType;
use App\Form\CktType;
use App\Form\OthersType;
use App\Form\HandoverType;
use App\Form\InnovType;
use App\Form\LessonType;
use App\Form\LgType;
use App\Form\ItvType;
use App\Form\CkormType;

class FormController extends AbstractController
{
    private $service_form;
    private $manager;

    public function __construct(FormService $service_form, ObjectManager $manager)
    {
        $this->service_form = $service_form;
        $this->manager = $manager;
    }

    public function processForm(Request $request, $entityForm, $type, $typeFormClass, $title, $mail_to = null, $title_mail = null){

        $entityForm->setType($type);

        $form = $this->createForm($typeFormClass, $entityForm);

        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()){

            $this->addFlash('success', Form::MSG_SUCCESS);

            $this->manager->persist($entityForm);

            if(!empty($mail_to)) $mail_to = $entityForm->{$mail_to}();
            if(!empty($title_mail)) $title = $title_mail;
            //envoi du mail
            $this->service_form->sendMail($type, $title, $entityForm, $mail_to);
            // pas de sauvegarde
            //$this->manager->flush();

            return $this->redirectToRoute('form_' . $type);

        }

        return $this->render('form/form_default.html.twig', [
            'title' => $title,
            'form' => $form->createView()
        ]);
    }

    /**
     * @Route("/", name="home")
     */
    public function index(Request $request, ObjectManager $manager)
    {
        return $this->render('form/index.html.twig', [
            'title' => 'Home',
        ]);
    }

    /**
     * @Route("/community-of-pratices", name="form_cop")
     */
    public function cop(Request $request)
    {

        $entityForm = new Form();

        return $this->processForm($request, $entityForm, Form::FORM_COP, CopType::class, 'Community of pratices', 'getManagerEmail');

    }

    /**
     * @Route("/critical-knowledge-transfer", name="form_ckt")
     */
    public function ckt(Request $request)
    {
        $entityForm = new Form();

        return $this->processForm($request, $entityForm, Form::FORM_CKT, CktType::class, 'Critical Knowledge Transfer', 'getGiverEmailManager');

    }

    /**
     * @Route("/handover", name="form_handover")
     */
    public function handover(Request $request, ObjectManager $manager)
    {
        $entityForm = new Form();

        return $this->processForm($request, $entityForm, Form::FORM_HANDOVER, HandoverType::class, 'Handover', 'getLeaverEmailManager');

    }

    /**
     * @Route("/innov", name="form_innov")
     */
    public function innov(Request $request, ObjectManager $manager)
    {
        $entityForm = new Form();

        return $this->processForm($request, $entityForm, Form::FORM_INNOV, InnovType::class, 'Innovation workshops', 'getManagerEmail');

    }

    /**
     * @Route("/learning-group", name="form_lg")
     */
    public function lg(Request $request, ObjectManager $manager)
    {
        $entityForm = new Form();

        return $this->processForm($request, $entityForm, Form::FORM_LG, LgType::class, 'Learning group', 'getManagerEmail');

    }

    /**
     * @Route("/lessons-learnt", name="form_lesson")
     */
    public function lesson(Request $request, ObjectManager $manager)
    {
        $entityForm = new Form();

        return $this->processForm($request, $entityForm, Form::FORM_LESSON, LessonType::class, 'Lessons learnt', 'getManagerEmail');

    }

    /**
     * @Route("/others", name="form_others")
     */
    public function others(Request $request, ObjectManager $manager)
    {
        $entityForm = new Form();

        $title = $request->query->get('title');

        if(!empty($title)){
            $entityForm->setTitle($title);
        }else{
            $title = '';
        }

        return $this->processForm($request, $entityForm, Form::FORM_OTHERS, OthersType::class, $title, 'getManagerEmail', 'Others - '. $title);

    }

    /**
     * @Route("/ckorm", name="form_ckorm")
     */
    public function ckorm(Request $request, ObjectManager $manager)
    {
        $entityForm = new Form();

        return $this->processForm($request, $entityForm, Form::FORM_CKORM, CkormType::class, 'CKO Risk Management','getManagerEmail');

    }

    /**
     * @Route("/itv", name="form_itv")
     */
    public function itv(Request $request, ObjectManager $manager)
    {
        $entityForm = new Form();

        return $this->processForm($request, $entityForm, Form::FORM_ITV, ItvType::class, 'Industrial Tutorial Video', 'getManagerEmail');

    }
}
