(window["webpackJsonp"] = window["webpackJsonp"] || []).push([[0],{

/***/ "./assets/js/add-collection-widget.js":
/*!********************************************!*\
  !*** ./assets/js/add-collection-widget.js ***!
  \********************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

/* WEBPACK VAR INJECTION */(function(jQuery, $) {jQuery(document).ready(function () {
  // recuperation des elements avec ajout de champ dynamique
  $collectionHolder = $('.multifield-dynamic'); // permet de rajouter un bouton delete dans l'item $formLi

  function addFormDeleteLink($formLi) {
    var $removeFormButton = $('<button type="button" class="btn-danger btn">Delete</button>');
    $formLi.append($removeFormButton);
    $removeFormButton.on('click', function (e) {
      // remove the li for the tag form
      $formLi.remove();
    });
  } // permet de rajouter un item dans "item_list" avec le bouton delete "add_delete - boolean"


  function addFormField(item_list, add_delete) {
    var list = item_list; // Try to find the counter of the list or use the length of the list

    var counter = list.data('widget-counter') | list.children().length; // grab the prototype template

    var newWidget = list.attr('data-prototype'); // replace the "__name__" used in the id and name of the prototype
    // with a number that's unique to your emails
    // end name attribute looks like name="contact[emails][2]"

    newWidget = newWidget.replace(/__name__/g, counter); // Increase the counter

    counter++; // And store it, the length cannot be used if deleting widgets is allowed

    list.data('widget-counter', counter); // create a new list element and add it to the list

    var item = jQuery(newWidget).appendTo(list); // ajout du delete ?

    if (add_delete) addFormDeleteLink(item);
  } // evenement sur le bouton ajouter


  jQuery('.add-another-collection-widget').click(function (e) {
    addFormField(jQuery(jQuery(this).attr('data-list-selector')), true);
  }); // processus de creation des champs multiples

  $collectionHolder.each(function () {
    var nb_item_init = jQuery(this).attr('nb-item-init');
    var id = jQuery(this).attr('id');
    var list = jQuery('#' + id); // s'il y a des champs affiché de base

    if (nb_item_init != undefined) {
      // rajout autant de champs sans delete dnas la limite "nb_item_init"
      if (list.children().length < parseInt(nb_item_init)) {
        for (var i = 1; i <= parseInt(nb_item_init); i++) {
          addFormField(list, false);
        }
      } // si il y a plus de champs affichés, rajout du bouton delete


      if (list.children().length > parseInt(nb_item_init)) {
        jQuery(this).find('.form-group:gt(' + (nb_item_init - 1) + ')').each(function () {
          addFormDeleteLink(jQuery(this));
        });
      }
    } else {
      // si pas de limite, tous les champs affichés ont un bouton delete
      jQuery(this).find('.form-group').each(function () {
        addFormDeleteLink(jQuery(this));
      });
    }
  });
});
/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(/*! jquery */ "./node_modules/jquery/dist/jquery.js"), __webpack_require__(/*! jquery */ "./node_modules/jquery/dist/jquery.js")))

/***/ }),

/***/ "./assets/js/script.js":
/*!*****************************!*\
  !*** ./assets/js/script.js ***!
  \*****************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

/* WEBPACK VAR INJECTION */(function($) {$(function () {
  // SUFFIX
  $('[data-suffix]').each(function () {
    var suffix = $(this).attr('data-suffix');
    var id = $(this).attr('id');
    var render_suffix = '<div class="input-group-append">';
    render_suffix += '<label class="input-group-text" for="' + id + '">' + suffix + '</label>';
    render_suffix += '</div>';
    $(this).wrap('<div class="input-group mb-3"></div>').parent().append(render_suffix);
  }); // SUFFIX
});
/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(/*! jquery */ "./node_modules/jquery/dist/jquery.js")))

/***/ }),

/***/ "./assets/js/toggle-form.js":
/*!**********************************!*\
  !*** ./assets/js/toggle-form.js ***!
  \**********************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

/* WEBPACK VAR INJECTION */(function($) {$(function () {
  // TOGGLE FORM
  // permet de faire le toggle (show / hide) sur le wrapper
  function toggle_form_visible(ele, type, is_checked) {
    var wrapper = ele.parents('.form-group');

    if (type == 'hide') {
      if (is_checked) {
        wrapper.hide();
      } else {
        wrapper.show();
      }
    }

    if (type == 'show') {
      if (is_checked) {
        wrapper.show();
      } else {
        wrapper.hide();
      }
    }
  } // permet de faire le "required" si data-toggle-required


  function toggle_form_require(ele, type, is_checked) {
    var required = ele.attr('data-toggle-required');
    var label = ele.parents('.form-group').find('label').first();

    if (required != undefined) {
      if (type == 'hide') {
        if (is_checked) {
          ele.removeAttr('required');
          label.removeClass('required');
        } else {
          ele.attr('required', 'required');
          label.addClass('required');
        }
      }

      if (type == 'show') {
        if (is_checked) {
          ele.attr('required', 'required');
          label.addClass('required');
        } else {
          ele.removeAttr('required');
          label.removeClass('required');
        }
      }
    }
  } // verifie si le type (data-toggle-checked) existe sinon alert


  function toggle_form_verification_type(type, ele) {
    if (type == undefined) {
      alert('Il manque l\'attribut "data-toggle-checked" pour l\'element "' + ele.attr('id') + '"');
    }
  } // retourne la valeur selectionné d'un element d'un formulaire "el"


  function toggle_form_get_value(el) {
    var type = el.is(':checkbox') ? 'checkbox' : 'select';

    if (type == 'checkbox') {
      return el.is(':checked') ? el.val() : '';
    } else {
      return el.val();
    }
  } // retourne true si la valeur de l'element formulaire (pilote) est egale a l'attribut "data-toggle-value" de l'element "el"
  // sinon false


  function toggle_form_get_checked(pilote, el) {
    var val = el.attr('data-toggle-value');
    return toggle_form_get_value(pilote) == val;
  }

  function toggle_form_init() {
    // permet de cacher ou non les elements [data-toggle-id] si la cible pilote est checké ou pas
    $('[data-toggle-id]').each(function () {
      var pilote = $('[data-toggle="' + $(this).attr('data-toggle-id') + '"]');
      var type = $(this).attr('data-toggle-checked');
      var is_checked = toggle_form_get_checked(pilote, $(this));
      toggle_form_verification_type(type, $(this));
      toggle_form_visible($(this), type, is_checked);
      toggle_form_require($(this), type, is_checked);
    }); // gestion du changement d'etat

    $('[data-toggle]').change(function () {
      console.log('change');
      var pilote = $(this);
      var cible = $('[data-toggle-id="' + $(this).attr('data-toggle') + '"]');
      cible.each(function () {
        var type = $(this).attr('data-toggle-checked');
        var is_checked = toggle_form_get_checked(pilote, $(this));
        toggle_form_verification_type(type, $(this));
        toggle_form_visible($(this), type, is_checked);
        toggle_form_require($(this), type, is_checked);
      });
    });
  }

  toggle_form_init(); // TOGGLE FORM
});
/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(/*! jquery */ "./node_modules/jquery/dist/jquery.js")))

/***/ })

}]);
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8vLi9hc3NldHMvanMvYWRkLWNvbGxlY3Rpb24td2lkZ2V0LmpzIiwid2VicGFjazovLy8uL2Fzc2V0cy9qcy9zY3JpcHQuanMiLCJ3ZWJwYWNrOi8vLy4vYXNzZXRzL2pzL3RvZ2dsZS1mb3JtLmpzIl0sIm5hbWVzIjpbImpRdWVyeSIsImRvY3VtZW50IiwicmVhZHkiLCIkY29sbGVjdGlvbkhvbGRlciIsIiQiLCJhZGRGb3JtRGVsZXRlTGluayIsIiRmb3JtTGkiLCIkcmVtb3ZlRm9ybUJ1dHRvbiIsImFwcGVuZCIsIm9uIiwiZSIsInJlbW92ZSIsImFkZEZvcm1GaWVsZCIsIml0ZW1fbGlzdCIsImFkZF9kZWxldGUiLCJsaXN0IiwiY291bnRlciIsImRhdGEiLCJjaGlsZHJlbiIsImxlbmd0aCIsIm5ld1dpZGdldCIsImF0dHIiLCJyZXBsYWNlIiwiaXRlbSIsImFwcGVuZFRvIiwiY2xpY2siLCJlYWNoIiwibmJfaXRlbV9pbml0IiwiaWQiLCJ1bmRlZmluZWQiLCJwYXJzZUludCIsImkiLCJmaW5kIiwic3VmZml4IiwicmVuZGVyX3N1ZmZpeCIsIndyYXAiLCJwYXJlbnQiLCJ0b2dnbGVfZm9ybV92aXNpYmxlIiwiZWxlIiwidHlwZSIsImlzX2NoZWNrZWQiLCJ3cmFwcGVyIiwicGFyZW50cyIsImhpZGUiLCJzaG93IiwidG9nZ2xlX2Zvcm1fcmVxdWlyZSIsInJlcXVpcmVkIiwibGFiZWwiLCJmaXJzdCIsInJlbW92ZUF0dHIiLCJyZW1vdmVDbGFzcyIsImFkZENsYXNzIiwidG9nZ2xlX2Zvcm1fdmVyaWZpY2F0aW9uX3R5cGUiLCJhbGVydCIsInRvZ2dsZV9mb3JtX2dldF92YWx1ZSIsImVsIiwiaXMiLCJ2YWwiLCJ0b2dnbGVfZm9ybV9nZXRfY2hlY2tlZCIsInBpbG90ZSIsInRvZ2dsZV9mb3JtX2luaXQiLCJjaGFuZ2UiLCJjb25zb2xlIiwibG9nIiwiY2libGUiXSwibWFwcGluZ3MiOiI7Ozs7Ozs7OztBQUFBQSx1REFBTSxDQUFDQyxRQUFELENBQU4sQ0FBaUJDLEtBQWpCLENBQXVCLFlBQVk7QUFDL0I7QUFDQUMsbUJBQWlCLEdBQUdDLENBQUMsQ0FBQyxxQkFBRCxDQUFyQixDQUYrQixDQUkvQjs7QUFDQSxXQUFTQyxpQkFBVCxDQUEyQkMsT0FBM0IsRUFBb0M7QUFDaEMsUUFBSUMsaUJBQWlCLEdBQUdILENBQUMsQ0FBQyw4REFBRCxDQUF6QjtBQUNBRSxXQUFPLENBQUNFLE1BQVIsQ0FBZUQsaUJBQWY7QUFFQUEscUJBQWlCLENBQUNFLEVBQWxCLENBQXFCLE9BQXJCLEVBQThCLFVBQVNDLENBQVQsRUFBWTtBQUN0QztBQUNBSixhQUFPLENBQUNLLE1BQVI7QUFDSCxLQUhEO0FBSUgsR0FiOEIsQ0FlL0I7OztBQUNBLFdBQVNDLFlBQVQsQ0FBc0JDLFNBQXRCLEVBQWlDQyxVQUFqQyxFQUE0QztBQUN4QyxRQUFJQyxJQUFJLEdBQUdGLFNBQVgsQ0FEd0MsQ0FFeEM7O0FBQ0EsUUFBSUcsT0FBTyxHQUFHRCxJQUFJLENBQUNFLElBQUwsQ0FBVSxnQkFBVixJQUE4QkYsSUFBSSxDQUFDRyxRQUFMLEdBQWdCQyxNQUE1RCxDQUh3QyxDQUt4Qzs7QUFDQSxRQUFJQyxTQUFTLEdBQUdMLElBQUksQ0FBQ00sSUFBTCxDQUFVLGdCQUFWLENBQWhCLENBTndDLENBUXhDO0FBQ0E7QUFDQTs7QUFDQUQsYUFBUyxHQUFHQSxTQUFTLENBQUNFLE9BQVYsQ0FBa0IsV0FBbEIsRUFBK0JOLE9BQS9CLENBQVosQ0FYd0MsQ0FheEM7O0FBQ0FBLFdBQU8sR0FkaUMsQ0FleEM7O0FBQ0FELFFBQUksQ0FBQ0UsSUFBTCxDQUFVLGdCQUFWLEVBQTRCRCxPQUE1QixFQWhCd0MsQ0FrQnhDOztBQUNBLFFBQUlPLElBQUksR0FBR3ZCLE1BQU0sQ0FBQ29CLFNBQUQsQ0FBTixDQUFrQkksUUFBbEIsQ0FBMkJULElBQTNCLENBQVgsQ0FuQndDLENBcUJ4Qzs7QUFDQSxRQUFHRCxVQUFILEVBQWVULGlCQUFpQixDQUFDa0IsSUFBRCxDQUFqQjtBQUNsQixHQXZDOEIsQ0F5Qy9COzs7QUFDQXZCLFFBQU0sQ0FBQyxnQ0FBRCxDQUFOLENBQXlDeUIsS0FBekMsQ0FBK0MsVUFBVWYsQ0FBVixFQUFhO0FBQ3hERSxnQkFBWSxDQUFDWixNQUFNLENBQUNBLE1BQU0sQ0FBQyxJQUFELENBQU4sQ0FBYXFCLElBQWIsQ0FBa0Isb0JBQWxCLENBQUQsQ0FBUCxFQUFrRCxJQUFsRCxDQUFaO0FBQ0gsR0FGRCxFQTFDK0IsQ0E4Qy9COztBQUNBbEIsbUJBQWlCLENBQUN1QixJQUFsQixDQUF1QixZQUFZO0FBRS9CLFFBQUlDLFlBQVksR0FBRzNCLE1BQU0sQ0FBQyxJQUFELENBQU4sQ0FBYXFCLElBQWIsQ0FBa0IsY0FBbEIsQ0FBbkI7QUFDQSxRQUFJTyxFQUFFLEdBQUc1QixNQUFNLENBQUMsSUFBRCxDQUFOLENBQWFxQixJQUFiLENBQWtCLElBQWxCLENBQVQ7QUFDQSxRQUFJTixJQUFJLEdBQUlmLE1BQU0sQ0FBQyxNQUFNNEIsRUFBUCxDQUFsQixDQUorQixDQU0vQjs7QUFDQSxRQUFHRCxZQUFZLElBQUlFLFNBQW5CLEVBQTZCO0FBQ3pCO0FBQ0EsVUFBR2QsSUFBSSxDQUFDRyxRQUFMLEdBQWdCQyxNQUFoQixHQUF5QlcsUUFBUSxDQUFDSCxZQUFELENBQXBDLEVBQW1EO0FBQy9DLGFBQUksSUFBSUksQ0FBQyxHQUFHLENBQVosRUFBZ0JBLENBQUMsSUFBSUQsUUFBUSxDQUFDSCxZQUFELENBQTdCLEVBQThDSSxDQUFDLEVBQS9DLEVBQWtEO0FBQzlDbkIsc0JBQVksQ0FBQ0csSUFBRCxFQUFPLEtBQVAsQ0FBWjtBQUNIO0FBQ0osT0FOd0IsQ0FRekI7OztBQUNBLFVBQUdBLElBQUksQ0FBQ0csUUFBTCxHQUFnQkMsTUFBaEIsR0FBeUJXLFFBQVEsQ0FBQ0gsWUFBRCxDQUFwQyxFQUFtRDtBQUMvQzNCLGNBQU0sQ0FBQyxJQUFELENBQU4sQ0FBYWdDLElBQWIsQ0FBa0IscUJBQXFCTCxZQUFZLEdBQUcsQ0FBcEMsSUFBeUMsR0FBM0QsRUFBZ0VELElBQWhFLENBQXFFLFlBQVc7QUFDNUVyQiwyQkFBaUIsQ0FBQ0wsTUFBTSxDQUFDLElBQUQsQ0FBUCxDQUFqQjtBQUNILFNBRkQ7QUFHSDtBQUVKLEtBZkQsTUFlSztBQUNEO0FBQ0FBLFlBQU0sQ0FBQyxJQUFELENBQU4sQ0FBYWdDLElBQWIsQ0FBa0IsYUFBbEIsRUFBaUNOLElBQWpDLENBQXNDLFlBQVc7QUFDN0NyQix5QkFBaUIsQ0FBQ0wsTUFBTSxDQUFDLElBQUQsQ0FBUCxDQUFqQjtBQUNILE9BRkQ7QUFHSDtBQUVKLEdBN0JEO0FBK0JILENBOUVELEU7Ozs7Ozs7Ozs7OztBQ0FBSSwwQ0FBQyxDQUFDLFlBQVc7QUFFVDtBQUNBQSxHQUFDLENBQUMsZUFBRCxDQUFELENBQW1Cc0IsSUFBbkIsQ0FBd0IsWUFBVTtBQUM5QixRQUFJTyxNQUFNLEdBQUc3QixDQUFDLENBQUMsSUFBRCxDQUFELENBQVFpQixJQUFSLENBQWEsYUFBYixDQUFiO0FBQ0EsUUFBSU8sRUFBRSxHQUFHeEIsQ0FBQyxDQUFDLElBQUQsQ0FBRCxDQUFRaUIsSUFBUixDQUFhLElBQWIsQ0FBVDtBQUVBLFFBQUlhLGFBQWEsR0FBRyxrQ0FBcEI7QUFDQUEsaUJBQWEsSUFBSSwwQ0FBMENOLEVBQTFDLEdBQStDLElBQS9DLEdBQXNESyxNQUF0RCxHQUErRCxVQUFoRjtBQUNBQyxpQkFBYSxJQUFJLFFBQWpCO0FBRUE5QixLQUFDLENBQUMsSUFBRCxDQUFELENBQVErQixJQUFSLENBQWEsc0NBQWIsRUFBcURDLE1BQXJELEdBQThENUIsTUFBOUQsQ0FBcUUwQixhQUFyRTtBQUNILEdBVEQsRUFIUyxDQWFUO0FBQ0gsQ0FkQSxDQUFELEM7Ozs7Ozs7Ozs7OztBQ0FBOUIsMENBQUMsQ0FBQyxZQUFXO0FBRVQ7QUFDQTtBQUNBLFdBQVNpQyxtQkFBVCxDQUE2QkMsR0FBN0IsRUFBa0NDLElBQWxDLEVBQXdDQyxVQUF4QyxFQUFtRDtBQUMvQyxRQUFJQyxPQUFPLEdBQUdILEdBQUcsQ0FBQ0ksT0FBSixDQUFZLGFBQVosQ0FBZDs7QUFDQSxRQUFHSCxJQUFJLElBQUksTUFBWCxFQUFrQjtBQUNkLFVBQUdDLFVBQUgsRUFBYztBQUNWQyxlQUFPLENBQUNFLElBQVI7QUFDSCxPQUZELE1BRUs7QUFDREYsZUFBTyxDQUFDRyxJQUFSO0FBQ0g7QUFDSjs7QUFDRCxRQUFHTCxJQUFJLElBQUksTUFBWCxFQUFrQjtBQUNkLFVBQUdDLFVBQUgsRUFBYztBQUNWQyxlQUFPLENBQUNHLElBQVI7QUFDSCxPQUZELE1BRUs7QUFDREgsZUFBTyxDQUFDRSxJQUFSO0FBQ0g7QUFDSjtBQUNKLEdBcEJRLENBc0JUOzs7QUFDQSxXQUFTRSxtQkFBVCxDQUE2QlAsR0FBN0IsRUFBa0NDLElBQWxDLEVBQXdDQyxVQUF4QyxFQUFtRDtBQUMvQyxRQUFJTSxRQUFRLEdBQUdSLEdBQUcsQ0FBQ2pCLElBQUosQ0FBUyxzQkFBVCxDQUFmO0FBQ0EsUUFBSTBCLEtBQUssR0FBR1QsR0FBRyxDQUFDSSxPQUFKLENBQVksYUFBWixFQUEyQlYsSUFBM0IsQ0FBZ0MsT0FBaEMsRUFBeUNnQixLQUF6QyxFQUFaOztBQUNBLFFBQUdGLFFBQVEsSUFBSWpCLFNBQWYsRUFBeUI7QUFDckIsVUFBR1UsSUFBSSxJQUFJLE1BQVgsRUFBa0I7QUFDZCxZQUFHQyxVQUFILEVBQWM7QUFDVkYsYUFBRyxDQUFDVyxVQUFKLENBQWUsVUFBZjtBQUNBRixlQUFLLENBQUNHLFdBQU4sQ0FBa0IsVUFBbEI7QUFDSCxTQUhELE1BR0s7QUFDRFosYUFBRyxDQUFDakIsSUFBSixDQUFTLFVBQVQsRUFBcUIsVUFBckI7QUFDQTBCLGVBQUssQ0FBQ0ksUUFBTixDQUFlLFVBQWY7QUFDSDtBQUNKOztBQUNELFVBQUdaLElBQUksSUFBSSxNQUFYLEVBQWtCO0FBQ2QsWUFBR0MsVUFBSCxFQUFjO0FBQ1ZGLGFBQUcsQ0FBQ2pCLElBQUosQ0FBUyxVQUFULEVBQXFCLFVBQXJCO0FBQ0EwQixlQUFLLENBQUNJLFFBQU4sQ0FBZSxVQUFmO0FBQ0gsU0FIRCxNQUdLO0FBQ0RiLGFBQUcsQ0FBQ1csVUFBSixDQUFlLFVBQWY7QUFDQUYsZUFBSyxDQUFDRyxXQUFOLENBQWtCLFVBQWxCO0FBQ0g7QUFDSjtBQUNKO0FBQ0osR0E5Q1EsQ0FnRFQ7OztBQUNBLFdBQVNFLDZCQUFULENBQXVDYixJQUF2QyxFQUE2Q0QsR0FBN0MsRUFBaUQ7QUFDN0MsUUFBR0MsSUFBSSxJQUFJVixTQUFYLEVBQXFCO0FBQ2pCd0IsV0FBSyxDQUFDLGtFQUFrRWYsR0FBRyxDQUFDakIsSUFBSixDQUFTLElBQVQsQ0FBbEUsR0FBbUYsR0FBcEYsQ0FBTDtBQUNIO0FBQ0osR0FyRFEsQ0F1RFQ7OztBQUNBLFdBQVNpQyxxQkFBVCxDQUErQkMsRUFBL0IsRUFBa0M7QUFDOUIsUUFBSWhCLElBQUksR0FBR2dCLEVBQUUsQ0FBQ0MsRUFBSCxDQUFNLFdBQU4sSUFBcUIsVUFBckIsR0FBa0MsUUFBN0M7O0FBRUEsUUFBR2pCLElBQUksSUFBSSxVQUFYLEVBQXNCO0FBQ2xCLGFBQU9nQixFQUFFLENBQUNDLEVBQUgsQ0FBTSxVQUFOLElBQW9CRCxFQUFFLENBQUNFLEdBQUgsRUFBcEIsR0FBK0IsRUFBdEM7QUFDSCxLQUZELE1BRUs7QUFDRCxhQUFPRixFQUFFLENBQUNFLEdBQUgsRUFBUDtBQUNIO0FBQ0osR0FoRVEsQ0FrRVQ7QUFDQTs7O0FBQ0EsV0FBU0MsdUJBQVQsQ0FBaUNDLE1BQWpDLEVBQXlDSixFQUF6QyxFQUE0QztBQUN4QyxRQUFJRSxHQUFHLEdBQUdGLEVBQUUsQ0FBQ2xDLElBQUgsQ0FBUSxtQkFBUixDQUFWO0FBRUEsV0FBT2lDLHFCQUFxQixDQUFDSyxNQUFELENBQXJCLElBQWlDRixHQUF4QztBQUNIOztBQUVELFdBQVNHLGdCQUFULEdBQTJCO0FBQ3ZCO0FBQ0F4RCxLQUFDLENBQUMsa0JBQUQsQ0FBRCxDQUFzQnNCLElBQXRCLENBQTJCLFlBQVU7QUFDakMsVUFBSWlDLE1BQU0sR0FBR3ZELENBQUMsQ0FBQyxtQkFBbUJBLENBQUMsQ0FBQyxJQUFELENBQUQsQ0FBUWlCLElBQVIsQ0FBYSxnQkFBYixDQUFuQixHQUFtRCxJQUFwRCxDQUFkO0FBQ0EsVUFBSWtCLElBQUksR0FBR25DLENBQUMsQ0FBQyxJQUFELENBQUQsQ0FBUWlCLElBQVIsQ0FBYSxxQkFBYixDQUFYO0FBQ0EsVUFBSW1CLFVBQVUsR0FBR2tCLHVCQUF1QixDQUFDQyxNQUFELEVBQVN2RCxDQUFDLENBQUMsSUFBRCxDQUFWLENBQXhDO0FBRUFnRCxtQ0FBNkIsQ0FBQ2IsSUFBRCxFQUFPbkMsQ0FBQyxDQUFDLElBQUQsQ0FBUixDQUE3QjtBQUVBaUMseUJBQW1CLENBQUNqQyxDQUFDLENBQUMsSUFBRCxDQUFGLEVBQVVtQyxJQUFWLEVBQWdCQyxVQUFoQixDQUFuQjtBQUNBSyx5QkFBbUIsQ0FBQ3pDLENBQUMsQ0FBQyxJQUFELENBQUYsRUFBVW1DLElBQVYsRUFBZ0JDLFVBQWhCLENBQW5CO0FBQ0gsS0FURCxFQUZ1QixDQWF2Qjs7QUFDQXBDLEtBQUMsQ0FBQyxlQUFELENBQUQsQ0FBbUJ5RCxNQUFuQixDQUEwQixZQUFVO0FBQ2hDQyxhQUFPLENBQUNDLEdBQVIsQ0FBWSxRQUFaO0FBQ0EsVUFBSUosTUFBTSxHQUFHdkQsQ0FBQyxDQUFDLElBQUQsQ0FBZDtBQUNBLFVBQUk0RCxLQUFLLEdBQUc1RCxDQUFDLENBQUMsc0JBQXNCQSxDQUFDLENBQUMsSUFBRCxDQUFELENBQVFpQixJQUFSLENBQWEsYUFBYixDQUF0QixHQUFtRCxJQUFwRCxDQUFiO0FBRUEyQyxXQUFLLENBQUN0QyxJQUFOLENBQVcsWUFBVTtBQUNqQixZQUFJYSxJQUFJLEdBQUduQyxDQUFDLENBQUMsSUFBRCxDQUFELENBQVFpQixJQUFSLENBQWEscUJBQWIsQ0FBWDtBQUNBLFlBQUltQixVQUFVLEdBQUdrQix1QkFBdUIsQ0FBQ0MsTUFBRCxFQUFTdkQsQ0FBQyxDQUFDLElBQUQsQ0FBVixDQUF4QztBQUVBZ0QscUNBQTZCLENBQUNiLElBQUQsRUFBT25DLENBQUMsQ0FBQyxJQUFELENBQVIsQ0FBN0I7QUFFQWlDLDJCQUFtQixDQUFDakMsQ0FBQyxDQUFDLElBQUQsQ0FBRixFQUFVbUMsSUFBVixFQUFnQkMsVUFBaEIsQ0FBbkI7QUFDQUssMkJBQW1CLENBQUN6QyxDQUFDLENBQUMsSUFBRCxDQUFGLEVBQVVtQyxJQUFWLEVBQWdCQyxVQUFoQixDQUFuQjtBQUNILE9BUkQ7QUFVSCxLQWZEO0FBZ0JIOztBQUVEb0Isa0JBQWdCLEdBMUdQLENBMkdUO0FBQ0gsQ0E1R0EsQ0FBRCxDIiwiZmlsZSI6IjAuanMiLCJzb3VyY2VzQ29udGVudCI6WyJqUXVlcnkoZG9jdW1lbnQpLnJlYWR5KGZ1bmN0aW9uICgpIHtcbiAgICAvLyByZWN1cGVyYXRpb24gZGVzIGVsZW1lbnRzIGF2ZWMgYWpvdXQgZGUgY2hhbXAgZHluYW1pcXVlXG4gICAgJGNvbGxlY3Rpb25Ib2xkZXIgPSAkKCcubXVsdGlmaWVsZC1keW5hbWljJyk7XG5cbiAgICAvLyBwZXJtZXQgZGUgcmFqb3V0ZXIgdW4gYm91dG9uIGRlbGV0ZSBkYW5zIGwnaXRlbSAkZm9ybUxpXG4gICAgZnVuY3Rpb24gYWRkRm9ybURlbGV0ZUxpbmsoJGZvcm1MaSkge1xuICAgICAgICB2YXIgJHJlbW92ZUZvcm1CdXR0b24gPSAkKCc8YnV0dG9uIHR5cGU9XCJidXR0b25cIiBjbGFzcz1cImJ0bi1kYW5nZXIgYnRuXCI+RGVsZXRlPC9idXR0b24+Jyk7XG4gICAgICAgICRmb3JtTGkuYXBwZW5kKCRyZW1vdmVGb3JtQnV0dG9uKTtcblxuICAgICAgICAkcmVtb3ZlRm9ybUJ1dHRvbi5vbignY2xpY2snLCBmdW5jdGlvbihlKSB7XG4gICAgICAgICAgICAvLyByZW1vdmUgdGhlIGxpIGZvciB0aGUgdGFnIGZvcm1cbiAgICAgICAgICAgICRmb3JtTGkucmVtb3ZlKCk7XG4gICAgICAgIH0pO1xuICAgIH1cblxuICAgIC8vIHBlcm1ldCBkZSByYWpvdXRlciB1biBpdGVtIGRhbnMgXCJpdGVtX2xpc3RcIiBhdmVjIGxlIGJvdXRvbiBkZWxldGUgXCJhZGRfZGVsZXRlIC0gYm9vbGVhblwiXG4gICAgZnVuY3Rpb24gYWRkRm9ybUZpZWxkKGl0ZW1fbGlzdCwgYWRkX2RlbGV0ZSl7XG4gICAgICAgIHZhciBsaXN0ID0gaXRlbV9saXN0O1xuICAgICAgICAvLyBUcnkgdG8gZmluZCB0aGUgY291bnRlciBvZiB0aGUgbGlzdCBvciB1c2UgdGhlIGxlbmd0aCBvZiB0aGUgbGlzdFxuICAgICAgICB2YXIgY291bnRlciA9IGxpc3QuZGF0YSgnd2lkZ2V0LWNvdW50ZXInKSB8IGxpc3QuY2hpbGRyZW4oKS5sZW5ndGg7XG5cbiAgICAgICAgLy8gZ3JhYiB0aGUgcHJvdG90eXBlIHRlbXBsYXRlXG4gICAgICAgIHZhciBuZXdXaWRnZXQgPSBsaXN0LmF0dHIoJ2RhdGEtcHJvdG90eXBlJyk7XG5cbiAgICAgICAgLy8gcmVwbGFjZSB0aGUgXCJfX25hbWVfX1wiIHVzZWQgaW4gdGhlIGlkIGFuZCBuYW1lIG9mIHRoZSBwcm90b3R5cGVcbiAgICAgICAgLy8gd2l0aCBhIG51bWJlciB0aGF0J3MgdW5pcXVlIHRvIHlvdXIgZW1haWxzXG4gICAgICAgIC8vIGVuZCBuYW1lIGF0dHJpYnV0ZSBsb29rcyBsaWtlIG5hbWU9XCJjb250YWN0W2VtYWlsc11bMl1cIlxuICAgICAgICBuZXdXaWRnZXQgPSBuZXdXaWRnZXQucmVwbGFjZSgvX19uYW1lX18vZywgY291bnRlcik7XG5cbiAgICAgICAgLy8gSW5jcmVhc2UgdGhlIGNvdW50ZXJcbiAgICAgICAgY291bnRlcisrO1xuICAgICAgICAvLyBBbmQgc3RvcmUgaXQsIHRoZSBsZW5ndGggY2Fubm90IGJlIHVzZWQgaWYgZGVsZXRpbmcgd2lkZ2V0cyBpcyBhbGxvd2VkXG4gICAgICAgIGxpc3QuZGF0YSgnd2lkZ2V0LWNvdW50ZXInLCBjb3VudGVyKTtcblxuICAgICAgICAvLyBjcmVhdGUgYSBuZXcgbGlzdCBlbGVtZW50IGFuZCBhZGQgaXQgdG8gdGhlIGxpc3RcbiAgICAgICAgdmFyIGl0ZW0gPSBqUXVlcnkobmV3V2lkZ2V0KS5hcHBlbmRUbyhsaXN0KTtcblxuICAgICAgICAvLyBham91dCBkdSBkZWxldGUgP1xuICAgICAgICBpZihhZGRfZGVsZXRlKSBhZGRGb3JtRGVsZXRlTGluayhpdGVtKTtcbiAgICB9XG5cbiAgICAvLyBldmVuZW1lbnQgc3VyIGxlIGJvdXRvbiBham91dGVyXG4gICAgalF1ZXJ5KCcuYWRkLWFub3RoZXItY29sbGVjdGlvbi13aWRnZXQnKS5jbGljayhmdW5jdGlvbiAoZSkge1xuICAgICAgICBhZGRGb3JtRmllbGQoalF1ZXJ5KGpRdWVyeSh0aGlzKS5hdHRyKCdkYXRhLWxpc3Qtc2VsZWN0b3InKSksIHRydWUpO1xuICAgIH0pO1xuXG4gICAgLy8gcHJvY2Vzc3VzIGRlIGNyZWF0aW9uIGRlcyBjaGFtcHMgbXVsdGlwbGVzXG4gICAgJGNvbGxlY3Rpb25Ib2xkZXIuZWFjaChmdW5jdGlvbiAoKSB7XG5cbiAgICAgICAgbGV0IG5iX2l0ZW1faW5pdCA9IGpRdWVyeSh0aGlzKS5hdHRyKCduYi1pdGVtLWluaXQnKTtcbiAgICAgICAgbGV0IGlkID0galF1ZXJ5KHRoaXMpLmF0dHIoJ2lkJyk7XG4gICAgICAgIGxldCBsaXN0ICA9IGpRdWVyeSgnIycgKyBpZCk7XG5cbiAgICAgICAgLy8gcydpbCB5IGEgZGVzIGNoYW1wcyBhZmZpY2jDqSBkZSBiYXNlXG4gICAgICAgIGlmKG5iX2l0ZW1faW5pdCAhPSB1bmRlZmluZWQpe1xuICAgICAgICAgICAgLy8gcmFqb3V0IGF1dGFudCBkZSBjaGFtcHMgc2FucyBkZWxldGUgZG5hcyBsYSBsaW1pdGUgXCJuYl9pdGVtX2luaXRcIlxuICAgICAgICAgICAgaWYobGlzdC5jaGlsZHJlbigpLmxlbmd0aCA8IHBhcnNlSW50KG5iX2l0ZW1faW5pdCkpe1xuICAgICAgICAgICAgICAgIGZvcihsZXQgaSA9IDEgOyBpIDw9IHBhcnNlSW50KG5iX2l0ZW1faW5pdCkgOyBpKyspe1xuICAgICAgICAgICAgICAgICAgICBhZGRGb3JtRmllbGQobGlzdCwgZmFsc2UpO1xuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgIH1cblxuICAgICAgICAgICAgLy8gc2kgaWwgeSBhIHBsdXMgZGUgY2hhbXBzIGFmZmljaMOpcywgcmFqb3V0IGR1IGJvdXRvbiBkZWxldGVcbiAgICAgICAgICAgIGlmKGxpc3QuY2hpbGRyZW4oKS5sZW5ndGggPiBwYXJzZUludChuYl9pdGVtX2luaXQpKXtcbiAgICAgICAgICAgICAgICBqUXVlcnkodGhpcykuZmluZCgnLmZvcm0tZ3JvdXA6Z3QoJyArIChuYl9pdGVtX2luaXQgLSAxKSArICcpJykuZWFjaChmdW5jdGlvbigpIHtcbiAgICAgICAgICAgICAgICAgICAgYWRkRm9ybURlbGV0ZUxpbmsoalF1ZXJ5KHRoaXMpKTtcbiAgICAgICAgICAgICAgICB9KTtcbiAgICAgICAgICAgIH1cblxuICAgICAgICB9ZWxzZXtcbiAgICAgICAgICAgIC8vIHNpIHBhcyBkZSBsaW1pdGUsIHRvdXMgbGVzIGNoYW1wcyBhZmZpY2jDqXMgb250IHVuIGJvdXRvbiBkZWxldGVcbiAgICAgICAgICAgIGpRdWVyeSh0aGlzKS5maW5kKCcuZm9ybS1ncm91cCcpLmVhY2goZnVuY3Rpb24oKSB7XG4gICAgICAgICAgICAgICAgYWRkRm9ybURlbGV0ZUxpbmsoalF1ZXJ5KHRoaXMpKTtcbiAgICAgICAgICAgIH0pO1xuICAgICAgICB9XG5cbiAgICB9KTtcblxufSk7IiwiJChmdW5jdGlvbigpIHtcblxuICAgIC8vIFNVRkZJWFxuICAgICQoJ1tkYXRhLXN1ZmZpeF0nKS5lYWNoKGZ1bmN0aW9uKCl7XG4gICAgICAgIGxldCBzdWZmaXggPSAkKHRoaXMpLmF0dHIoJ2RhdGEtc3VmZml4Jyk7XG4gICAgICAgIGxldCBpZCA9ICQodGhpcykuYXR0cignaWQnKTtcblxuICAgICAgICBsZXQgcmVuZGVyX3N1ZmZpeCA9ICc8ZGl2IGNsYXNzPVwiaW5wdXQtZ3JvdXAtYXBwZW5kXCI+JztcbiAgICAgICAgcmVuZGVyX3N1ZmZpeCArPSAnPGxhYmVsIGNsYXNzPVwiaW5wdXQtZ3JvdXAtdGV4dFwiIGZvcj1cIicgKyBpZCArICdcIj4nICsgc3VmZml4ICsgJzwvbGFiZWw+JztcbiAgICAgICAgcmVuZGVyX3N1ZmZpeCArPSAnPC9kaXY+JztcblxuICAgICAgICAkKHRoaXMpLndyYXAoJzxkaXYgY2xhc3M9XCJpbnB1dC1ncm91cCBtYi0zXCI+PC9kaXY+JykucGFyZW50KCkuYXBwZW5kKHJlbmRlcl9zdWZmaXgpO1xuICAgIH0pXG4gICAgLy8gU1VGRklYXG59KTsiLCIkKGZ1bmN0aW9uKCkge1xuXG4gICAgLy8gVE9HR0xFIEZPUk1cbiAgICAvLyBwZXJtZXQgZGUgZmFpcmUgbGUgdG9nZ2xlIChzaG93IC8gaGlkZSkgc3VyIGxlIHdyYXBwZXJcbiAgICBmdW5jdGlvbiB0b2dnbGVfZm9ybV92aXNpYmxlKGVsZSwgdHlwZSwgaXNfY2hlY2tlZCl7XG4gICAgICAgIGxldCB3cmFwcGVyID0gZWxlLnBhcmVudHMoJy5mb3JtLWdyb3VwJyk7XG4gICAgICAgIGlmKHR5cGUgPT0gJ2hpZGUnKXtcbiAgICAgICAgICAgIGlmKGlzX2NoZWNrZWQpe1xuICAgICAgICAgICAgICAgIHdyYXBwZXIuaGlkZSgpO1xuICAgICAgICAgICAgfWVsc2V7XG4gICAgICAgICAgICAgICAgd3JhcHBlci5zaG93KCk7XG4gICAgICAgICAgICB9XG4gICAgICAgIH1cbiAgICAgICAgaWYodHlwZSA9PSAnc2hvdycpe1xuICAgICAgICAgICAgaWYoaXNfY2hlY2tlZCl7XG4gICAgICAgICAgICAgICAgd3JhcHBlci5zaG93KCk7XG4gICAgICAgICAgICB9ZWxzZXtcbiAgICAgICAgICAgICAgICB3cmFwcGVyLmhpZGUoKTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgfVxuICAgIH1cblxuICAgIC8vIHBlcm1ldCBkZSBmYWlyZSBsZSBcInJlcXVpcmVkXCIgc2kgZGF0YS10b2dnbGUtcmVxdWlyZWRcbiAgICBmdW5jdGlvbiB0b2dnbGVfZm9ybV9yZXF1aXJlKGVsZSwgdHlwZSwgaXNfY2hlY2tlZCl7XG4gICAgICAgIGxldCByZXF1aXJlZCA9IGVsZS5hdHRyKCdkYXRhLXRvZ2dsZS1yZXF1aXJlZCcpO1xuICAgICAgICBsZXQgbGFiZWwgPSBlbGUucGFyZW50cygnLmZvcm0tZ3JvdXAnKS5maW5kKCdsYWJlbCcpLmZpcnN0KCk7XG4gICAgICAgIGlmKHJlcXVpcmVkICE9IHVuZGVmaW5lZCl7XG4gICAgICAgICAgICBpZih0eXBlID09ICdoaWRlJyl7XG4gICAgICAgICAgICAgICAgaWYoaXNfY2hlY2tlZCl7XG4gICAgICAgICAgICAgICAgICAgIGVsZS5yZW1vdmVBdHRyKCdyZXF1aXJlZCcpO1xuICAgICAgICAgICAgICAgICAgICBsYWJlbC5yZW1vdmVDbGFzcygncmVxdWlyZWQnKTtcbiAgICAgICAgICAgICAgICB9ZWxzZXtcbiAgICAgICAgICAgICAgICAgICAgZWxlLmF0dHIoJ3JlcXVpcmVkJywgJ3JlcXVpcmVkJyk7XG4gICAgICAgICAgICAgICAgICAgIGxhYmVsLmFkZENsYXNzKCdyZXF1aXJlZCcpO1xuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIGlmKHR5cGUgPT0gJ3Nob3cnKXtcbiAgICAgICAgICAgICAgICBpZihpc19jaGVja2VkKXtcbiAgICAgICAgICAgICAgICAgICAgZWxlLmF0dHIoJ3JlcXVpcmVkJywgJ3JlcXVpcmVkJyk7XG4gICAgICAgICAgICAgICAgICAgIGxhYmVsLmFkZENsYXNzKCdyZXF1aXJlZCcpO1xuICAgICAgICAgICAgICAgIH1lbHNle1xuICAgICAgICAgICAgICAgICAgICBlbGUucmVtb3ZlQXR0cigncmVxdWlyZWQnKTtcbiAgICAgICAgICAgICAgICAgICAgbGFiZWwucmVtb3ZlQ2xhc3MoJ3JlcXVpcmVkJyk7XG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgfVxuICAgICAgICB9XG4gICAgfVxuXG4gICAgLy8gdmVyaWZpZSBzaSBsZSB0eXBlIChkYXRhLXRvZ2dsZS1jaGVja2VkKSBleGlzdGUgc2lub24gYWxlcnRcbiAgICBmdW5jdGlvbiB0b2dnbGVfZm9ybV92ZXJpZmljYXRpb25fdHlwZSh0eXBlLCBlbGUpe1xuICAgICAgICBpZih0eXBlID09IHVuZGVmaW5lZCl7XG4gICAgICAgICAgICBhbGVydCgnSWwgbWFucXVlIGxcXCdhdHRyaWJ1dCBcImRhdGEtdG9nZ2xlLWNoZWNrZWRcIiBwb3VyIGxcXCdlbGVtZW50IFwiJyArIGVsZS5hdHRyKCdpZCcpICsgJ1wiJyk7XG4gICAgICAgIH1cbiAgICB9XG5cbiAgICAvLyByZXRvdXJuZSBsYSB2YWxldXIgc2VsZWN0aW9ubsOpIGQndW4gZWxlbWVudCBkJ3VuIGZvcm11bGFpcmUgXCJlbFwiXG4gICAgZnVuY3Rpb24gdG9nZ2xlX2Zvcm1fZ2V0X3ZhbHVlKGVsKXtcbiAgICAgICAgbGV0IHR5cGUgPSBlbC5pcygnOmNoZWNrYm94JykgPyAnY2hlY2tib3gnIDogJ3NlbGVjdCc7XG5cbiAgICAgICAgaWYodHlwZSA9PSAnY2hlY2tib3gnKXtcbiAgICAgICAgICAgIHJldHVybiBlbC5pcygnOmNoZWNrZWQnKSA/IGVsLnZhbCgpIDogJyc7XG4gICAgICAgIH1lbHNle1xuICAgICAgICAgICAgcmV0dXJuIGVsLnZhbCgpO1xuICAgICAgICB9XG4gICAgfVxuXG4gICAgLy8gcmV0b3VybmUgdHJ1ZSBzaSBsYSB2YWxldXIgZGUgbCdlbGVtZW50IGZvcm11bGFpcmUgKHBpbG90ZSkgZXN0IGVnYWxlIGEgbCdhdHRyaWJ1dCBcImRhdGEtdG9nZ2xlLXZhbHVlXCIgZGUgbCdlbGVtZW50IFwiZWxcIlxuICAgIC8vIHNpbm9uIGZhbHNlXG4gICAgZnVuY3Rpb24gdG9nZ2xlX2Zvcm1fZ2V0X2NoZWNrZWQocGlsb3RlLCBlbCl7XG4gICAgICAgIGxldCB2YWwgPSBlbC5hdHRyKCdkYXRhLXRvZ2dsZS12YWx1ZScpO1xuXG4gICAgICAgIHJldHVybiB0b2dnbGVfZm9ybV9nZXRfdmFsdWUocGlsb3RlKSA9PSB2YWw7XG4gICAgfVxuXG4gICAgZnVuY3Rpb24gdG9nZ2xlX2Zvcm1faW5pdCgpe1xuICAgICAgICAvLyBwZXJtZXQgZGUgY2FjaGVyIG91IG5vbiBsZXMgZWxlbWVudHMgW2RhdGEtdG9nZ2xlLWlkXSBzaSBsYSBjaWJsZSBwaWxvdGUgZXN0IGNoZWNrw6kgb3UgcGFzXG4gICAgICAgICQoJ1tkYXRhLXRvZ2dsZS1pZF0nKS5lYWNoKGZ1bmN0aW9uKCl7XG4gICAgICAgICAgICBsZXQgcGlsb3RlID0gJCgnW2RhdGEtdG9nZ2xlPVwiJyArICQodGhpcykuYXR0cignZGF0YS10b2dnbGUtaWQnKSArJ1wiXScpO1xuICAgICAgICAgICAgbGV0IHR5cGUgPSAkKHRoaXMpLmF0dHIoJ2RhdGEtdG9nZ2xlLWNoZWNrZWQnKTtcbiAgICAgICAgICAgIGxldCBpc19jaGVja2VkID0gdG9nZ2xlX2Zvcm1fZ2V0X2NoZWNrZWQocGlsb3RlLCAkKHRoaXMpKTtcblxuICAgICAgICAgICAgdG9nZ2xlX2Zvcm1fdmVyaWZpY2F0aW9uX3R5cGUodHlwZSwgJCh0aGlzKSk7XG5cbiAgICAgICAgICAgIHRvZ2dsZV9mb3JtX3Zpc2libGUoJCh0aGlzKSwgdHlwZSwgaXNfY2hlY2tlZCk7XG4gICAgICAgICAgICB0b2dnbGVfZm9ybV9yZXF1aXJlKCQodGhpcyksIHR5cGUsIGlzX2NoZWNrZWQpO1xuICAgICAgICB9KTtcblxuICAgICAgICAvLyBnZXN0aW9uIGR1IGNoYW5nZW1lbnQgZCdldGF0XG4gICAgICAgICQoJ1tkYXRhLXRvZ2dsZV0nKS5jaGFuZ2UoZnVuY3Rpb24oKXtcbiAgICAgICAgICAgIGNvbnNvbGUubG9nKCdjaGFuZ2UnKVxuICAgICAgICAgICAgbGV0IHBpbG90ZSA9ICQodGhpcyk7XG4gICAgICAgICAgICBsZXQgY2libGUgPSAkKCdbZGF0YS10b2dnbGUtaWQ9XCInICsgJCh0aGlzKS5hdHRyKCdkYXRhLXRvZ2dsZScpICsnXCJdJyk7XG5cbiAgICAgICAgICAgIGNpYmxlLmVhY2goZnVuY3Rpb24oKXtcbiAgICAgICAgICAgICAgICBsZXQgdHlwZSA9ICQodGhpcykuYXR0cignZGF0YS10b2dnbGUtY2hlY2tlZCcpO1xuICAgICAgICAgICAgICAgIGxldCBpc19jaGVja2VkID0gdG9nZ2xlX2Zvcm1fZ2V0X2NoZWNrZWQocGlsb3RlLCAkKHRoaXMpKTtcblxuICAgICAgICAgICAgICAgIHRvZ2dsZV9mb3JtX3ZlcmlmaWNhdGlvbl90eXBlKHR5cGUsICQodGhpcykpO1xuXG4gICAgICAgICAgICAgICAgdG9nZ2xlX2Zvcm1fdmlzaWJsZSgkKHRoaXMpLCB0eXBlLCBpc19jaGVja2VkKTtcbiAgICAgICAgICAgICAgICB0b2dnbGVfZm9ybV9yZXF1aXJlKCQodGhpcyksIHR5cGUsIGlzX2NoZWNrZWQpO1xuICAgICAgICAgICAgfSk7XG5cbiAgICAgICAgfSk7XG4gICAgfVxuXG4gICAgdG9nZ2xlX2Zvcm1faW5pdCgpO1xuICAgIC8vIFRPR0dMRSBGT1JNXG59KTsiXSwic291cmNlUm9vdCI6IiJ9