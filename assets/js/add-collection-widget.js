jQuery(document).ready(function () {
    // recuperation des elements avec ajout de champ dynamique
    $collectionHolder = $('.multifield-dynamic');

    // permet de rajouter un bouton delete dans l'item $formLi
    function addFormDeleteLink($formLi) {
        var $removeFormButton = $('<button type="button" class="btn-danger btn">Delete</button>');
        $formLi.append($removeFormButton);

        $removeFormButton.on('click', function(e) {
            // remove the li for the tag form
            $formLi.remove();
        });
    }

    // permet de rajouter un item dans "item_list" avec le bouton delete "add_delete - boolean"
    function addFormField(item_list, add_delete){
        var list = item_list;
        // Try to find the counter of the list or use the length of the list
        var counter = list.data('widget-counter') | list.children().length;

        // grab the prototype template
        var newWidget = list.attr('data-prototype');

        // replace the "__name__" used in the id and name of the prototype
        // with a number that's unique to your emails
        // end name attribute looks like name="contact[emails][2]"
        newWidget = newWidget.replace(/__name__/g, counter);

        // Increase the counter
        counter++;
        // And store it, the length cannot be used if deleting widgets is allowed
        list.data('widget-counter', counter);

        // create a new list element and add it to the list
        var item = jQuery(newWidget).appendTo(list);

        // ajout du delete ?
        if(add_delete) addFormDeleteLink(item);
    }

    // evenement sur le bouton ajouter
    jQuery('.add-another-collection-widget').click(function (e) {
        addFormField(jQuery(jQuery(this).attr('data-list-selector')), true);
    });

    // processus de creation des champs multiples
    $collectionHolder.each(function () {

        let nb_item_init = jQuery(this).attr('nb-item-init');
        let id = jQuery(this).attr('id');
        let list  = jQuery('#' + id);

        // s'il y a des champs affiché de base
        if(nb_item_init != undefined){
            // rajout autant de champs sans delete dnas la limite "nb_item_init"
            if(list.children().length < parseInt(nb_item_init)){
                for(let i = 1 ; i <= parseInt(nb_item_init) ; i++){
                    addFormField(list, false);
                }
            }

            // si il y a plus de champs affichés, rajout du bouton delete
            if(list.children().length > parseInt(nb_item_init)){
                jQuery(this).find('.form-group:gt(' + (nb_item_init - 1) + ')').each(function() {
                    addFormDeleteLink(jQuery(this));
                });
            }

        }else{
            // si pas de limite, tous les champs affichés ont un bouton delete
            jQuery(this).find('.form-group').each(function() {
                addFormDeleteLink(jQuery(this));
            });
        }

    });

});