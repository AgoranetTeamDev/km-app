$(function() {

    // TOGGLE FORM
    // permet de faire le toggle (show / hide) sur le wrapper
    function toggle_form_visible(ele, type, is_checked){
        let wrapper = ele.parents('.form-group');
        if(type == 'hide'){
            if(is_checked){
                wrapper.hide();
            }else{
                wrapper.show();
            }
        }
        if(type == 'show'){
            if(is_checked){
                wrapper.show();
            }else{
                wrapper.hide();
            }
        }
    }

    // permet de faire le "required" si data-toggle-required
    function toggle_form_require(ele, type, is_checked){
        let required = ele.attr('data-toggle-required');
        let label = ele.parents('.form-group').find('label').first();
        if(required != undefined){
            if(type == 'hide'){
                if(is_checked){
                    ele.removeAttr('required');
                    label.removeClass('required');
                }else{
                    ele.attr('required', 'required');
                    label.addClass('required');
                }
            }
            if(type == 'show'){
                if(is_checked){
                    ele.attr('required', 'required');
                    label.addClass('required');
                }else{
                    ele.removeAttr('required');
                    label.removeClass('required');
                }
            }
        }
    }

    // verifie si le type (data-toggle-checked) existe sinon alert
    function toggle_form_verification_type(type, ele){
        if(type == undefined){
            alert('Il manque l\'attribut "data-toggle-checked" pour l\'element "' + ele.attr('id') + '"');
        }
    }

    // retourne la valeur selectionné d'un element d'un formulaire "el"
    function toggle_form_get_value(el){
        let type = el.is(':checkbox') ? 'checkbox' : 'select';

        if(type == 'checkbox'){
            return el.is(':checked') ? el.val() : '';
        }else{
            return el.val();
        }
    }

    // retourne true si la valeur de l'element formulaire (pilote) est egale a l'attribut "data-toggle-value" de l'element "el"
    // sinon false
    function toggle_form_get_checked(pilote, el){
        let val = el.attr('data-toggle-value');

        return toggle_form_get_value(pilote) == val;
    }

    function toggle_form_init(){
        // permet de cacher ou non les elements [data-toggle-id] si la cible pilote est checké ou pas
        $('[data-toggle-id]').each(function(){
            let pilote = $('[data-toggle="' + $(this).attr('data-toggle-id') +'"]');
            let type = $(this).attr('data-toggle-checked');
            let is_checked = toggle_form_get_checked(pilote, $(this));

            toggle_form_verification_type(type, $(this));

            toggle_form_visible($(this), type, is_checked);
            toggle_form_require($(this), type, is_checked);
        });

        // gestion du changement d'etat
        $('[data-toggle]').change(function(){
            console.log('change')
            let pilote = $(this);
            let cible = $('[data-toggle-id="' + $(this).attr('data-toggle') +'"]');

            cible.each(function(){
                let type = $(this).attr('data-toggle-checked');
                let is_checked = toggle_form_get_checked(pilote, $(this));

                toggle_form_verification_type(type, $(this));

                toggle_form_visible($(this), type, is_checked);
                toggle_form_require($(this), type, is_checked);
            });

        });
    }

    toggle_form_init();
    // TOGGLE FORM
});