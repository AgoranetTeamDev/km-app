$(function() {

    // SUFFIX
    $('[data-suffix]').each(function(){
        let suffix = $(this).attr('data-suffix');
        let id = $(this).attr('id');

        let render_suffix = '<div class="input-group-append">';
        render_suffix += '<label class="input-group-text" for="' + id + '">' + suffix + '</label>';
        render_suffix += '</div>';

        $(this).wrap('<div class="input-group mb-3"></div>').parent().append(render_suffix);
    })
    // SUFFIX
});