// assets/js/app.js
require("@babel/polyfill");
require('../scss/bootstrap.scss');
require('../scss/variables.scss');
require('../scss/app.scss');

// loads the jquery package from node_modules
var $ = require('jquery');

// import the function from script.js (the .js extension is optional)
// ./ (or ../) means to look for a local file
//var script = require(['./script', './add-collection-widget', './toggle-form', './debug-required']);
var script = require(['./script', './add-collection-widget', './toggle-form']);

//console.log('Hello Webpack Encore');