#install
composer install
create database
migrate: php bin/console doctrine:migrations:migrate

command(s):
php bin/console cache:clear
php bin/console list make
php bin/console make:form

#Symfony Encore - Webpack
https://symfony.com/doc/current/frontend/encore/installation.html
installer : yarn add sass-loader@^7.0.1 node-sass --dev
start/restart encore:
yarn encore dev --watch
write your code in assets/
compilation from webpack generate files in public/build

#SASS
enable: https://symfony.com/doc/current/frontend/encore/css-preprocessors.html
config: https://symfony.com/doc/current/frontend/encore/simple-example.html
