#!/usr/bin/env bash

DIRECTORY="`dirname $0`/"

DOCKER="/usr/local/bin/docker"
DOCKER_COMPOSE="/usr/local/bin/docker-compose"
CONSOLE="bin/console"
IMAGE_ID=$(cat $DIRECTORY.env | grep 'COMPOSE_PREFIX_PROJECT_NAME=' | awk '{split($0, out, "="); print out[2]}')
DEFAULT_ENV=$(cat $DIRECTORY.env | grep 'DEFAULT_ENV=' | awk '{split($0, out, "="); print out[2]}')
CONTAINER_NAME=$(docker ps | grep -v "CONTAINER ID"| grep "$IMAGE_ID" | awk '{ print $NF }')


echo ""
echo "*****************************"
echo "*     ACTUAL DOCKER CONTAINER"
echo "*****************************"

PS_COMMAND="docker ps | grep $CONTAINER_NAME"
eval $PS_COMMAND

if [ ! -f $DOCKER ]; then
    echo "docker command not found ($DOCKER)!"
    exit 1;
fi
if [ ! -f $DOCKER_COMPOSE ]; then
    echo "docker command not found ($DOCKER_COMPOSE)!"
    exit 1;
fi
if [ ! -f "$DIRECTORY/../"$CONSOLE ]; then
    echo "Symfony4 console command not found ($CONSOLE)!"
    exit 1;
fi

if [ "" == "$CONTAINER_NAME" ]; then
CONTAINER_NAME="$IMAGE_ID""$DEFAULT_ENV"
fi

echo ""

case "$1" in
start)
        COMMAND="$DOCKER start $CONTAINER_NAME"
        ;;
stop)
        COMMAND="$DOCKER stop $CONTAINER_NAME"
        ;;
status)
        COMMAND="$DOCKER stats $CONTAINER_NAME"
        ;;
stats)
        COMMAND="$DOCKER stats $CONTAINER_NAME"
        ;;
log)
        COMMAND="$DOCKER logs $CONTAINER_NAME"
        ;;
logs)
        COMMAND="$DOCKER logs --follow $CONTAINER_NAME"
        ;;
ps)
        COMMAND="$DOCKER ps | grep $IMAGE_ID"
        ;;
build)
        cd $DIRECTORY
        COMMAND="$DOCKER_COMPOSE up -d --build && echo ''&& ($DOCKER ps | grep $IMAGE_ID)"
        ;;
bash)
        COMMAND="clear && $DOCKER exec -it $CONTAINER_NAME bash"
        ;;
build-with-remove)
        cd $DIRECTORY
        COMMAND="$DOCKER stop $CONTAINER_NAME && \
        $DOCKER rm $CONTAINER_NAME && \
        $DOCKER_COMPOSE up -d --build --force-recreate --remove-orphans && \
        echo ''&& ($DOCKER ps | grep $IMAGE_ID)"
        ;;
*)
        echo "Usage: ""$DIRECTORY""do.sh {start|stop|status|log|logs|ps|build|bash|build-with-remove}"
        echo ""
        exit 2
        ;;
esac

echo ""
echo "*****************************"
echo "*     LAUNCHING COMMANDE"
echo "*****************************"
echo ""
echo "Command is : '$COMMAND'"
echo ""
echo ""
echo "*****************************"
echo "*     RESULT"
echo "*****************************"
echo ""
eval $COMMAND
echo ""
echo ""

exit 0




